package cdc.test.perfs.instrument.asm;

public class D1 {
    // public static void s0() {
    // System.out.println("s0()");
    // }

    // 1 boolean/Boolean

    public static void sz(boolean v) {
        System.out.println("sz(" + v + ")");
    }

    public static void sZ(Boolean v) {
        System.out.println("sZ(" + v + ")");
    }

    public void vz(boolean v) {
        System.out.println("vz(" + v + ")");
    }

    public void vZ(Boolean v) {
        System.out.println("vZ(" + v + ")");
    }

    public static void szz(boolean v1,
                           boolean v2) {
        System.out.println("szz(" + v1 + ", " + v2 + ")");
    }

    public static void szZ(boolean v1,
                           Boolean v2) {
        System.out.println("szZ(" + v1 + ", " + v2 + ")");
    }

    public static void sZz(Boolean v1,
                           boolean v2) {
        System.out.println("sZz(" + v1 + ", " + v2 + ")");
    }

    public static void sZZ(Boolean v1,
                           Boolean v2) {
        System.out.println("sZZ(" + v1 + ", " + v2 + ")");
    }

    public void vzz(boolean v1,
                    boolean v2) {
        System.out.println("vzz(" + v1 + ", " + v2 + ")");
    }

    public void vzZ(boolean v1,
                    Boolean v2) {
        System.out.println("vzZ(" + v1 + ", " + v2 + ")");
    }

    public void vZz(Boolean v1,
                    boolean v2) {
        System.out.println("vZz(" + v1 + ", " + v2 + ")");
    }

    public void vZZ(Boolean v1,
                    Boolean v2) {
        System.out.println("vZZ(" + v1 + ", " + v2 + ")");
    }

    // 1 char/Character

    public static void sc(char v) {
        System.out.println("sc(" + v + ")");
    }

    public static void sC(Character v) {
        System.out.println("sC(" + v + ")");
    }

    public void vc(char v) {
        System.out.println("vc(" + v + ")");
    }

    public void vC(Character v) {
        System.out.println("vC(" + v + ")");
    }

    // 1 byte/Byte

    public static void sb(byte v) {
        System.out.println("sb(" + v + ")");
    }

    public static void sB(Byte v) {
        System.out.println("sB(" + v + ")");
    }

    public void vb(byte v) {
        System.out.println("vb(" + v + ")");
    }

    public void vB(Byte v) {
        System.out.println("vB(" + v + ")");
    }

    // 1 short/Short

    public static void ss(short v) {
        System.out.println("ss(" + v + ")");
    }

    public static void sS(Short v) {
        System.out.println("sS(" + v + ")");
    }

    public void vs(short v) {
        System.out.println("vs(" + v + ")");
    }

    public void vS(Short v) {
        System.out.println("vS(" + v + ")");
    }

    // 1 int/Integer

    public static void si(int v) {
        System.out.println("si(" + v + ")");
    }

    public static void sI(Integer v) {
        System.out.println("sI(" + v + ")");
    }

    public void vi(int v) {
        System.out.println("vi(" + v + ")");
    }

    public void vI(Integer v) {
        System.out.println("vI(" + v + ")");
    }

    // 1 long/Long

    public static void sj(long v) {
        System.out.println("sj(" + v + ")");
    }

    public static void sJ(Long v) {
        System.out.println("sJ(" + v + ")");
    }

    public void vj(long v) {
        System.out.println("vj(" + v + ")");
    }

    public void vJ(Long v) {
        System.out.println("vJ(" + v + ")");
    }

    // 1 float/Float

    public static void sf(float v) {
        System.out.println("sf(" + v + ")");
    }

    public static void sF(Float v) {
        System.out.println("sF(" + v + ")");
    }

    public void vf(float v) {
        System.out.println("vf(" + v + ")");
    }

    public void vF(Float v) {
        System.out.println("vF(" + v + ")");
    }

    // 1 double/Double

    public static void sd(double v) {
        System.out.println("sd(" + v + ")");
    }

    public static void sD(Double v) {
        System.out.println("sD(" + v + ")");
    }

    public void vd(double v) {
        System.out.println("vd(" + v + ")");
    }

    public void vD(Double v) {
        System.out.println("vD(" + v + ")");
    }

    // 1 String

    public static void sString(String v) {
        System.out.println("sString(" + v + ")");
    }

    public void vString(String v) {
        System.out.println("vString(" + v + ")");
    }

    // Others

    public void m0() {
        System.out.println("m1()");
        throw new IllegalArgumentException();
    }

    public void m1() {
        System.out.println("m1()");
        Nested.m2("Hello");
    }

    public void m2(String s) {
        System.out.println("m2(" + s + ")");
        m3("Hello", 10.0f);
        m1();
        m1s();
        sz(true);
        sZ(true);
        szz(true, false);
        szZ(true, false);
        sZz(true, false);
        sZZ(true, false);
        vz(false);
        vZ(false);
        sc('a');
        sC('a');
        sb((byte) 10);
        sB((byte) 10);
        ss((short) 10);
        sS((short) 10);
        si(10);
        sI(10);
        sj(10L);
        sJ(10L);
        sf(10.1f);
        sF(10.2f);
        sd(10.1);
        sD(10.2);
        try {
            m0();
        } catch (final Exception e) {
            System.out.println("exception raised in m0");
        }
    }

    public void m3(String s,
                   float f) {
        System.out.println("m3(" + s + ", " + f + ")");
    }

    public static void m1s() {
        System.out.println("m1s()");
        m2s("Hello");
        m3s("Hello", 10.0f);
    }

    public static void m2s(String s) {
        System.out.println("m2s(" + s + ")");
    }

    public static void m3s1(String s,
                            Float f) {
        System.out.println("m3s1(" + s + ", " + f + ")");
    }

    public static void m3s(String s,
                           float f) {
        System.out.println("m3s(" + s + ", " + f + ")");
    }

    // private static void randomThrow() {
    // final Random r = new Random();
    // if (r.nextInt(100) < 50) {
    // LOGGER.info("THROW an exception");
    // throw new IllegalArgumentException();
    // } else {
    // LOGGER.info("DON'T THROW an exception");
    // }
    // }

    private static class Nested {
        public static void m2(String s) {
            System.out.println("Nested.m2(" + s + ")");
        }
    }
}