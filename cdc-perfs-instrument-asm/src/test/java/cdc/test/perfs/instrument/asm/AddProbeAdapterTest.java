package cdc.test.perfs.instrument.asm;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.io.utils.NonCloseableOutputStream;
import cdc.io.xml.XmlWriter;
import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.Source;
import cdc.perfs.core.io.PerfsXml;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.perfs.instrument.Configurator;
import cdc.perfs.instrument.asm.Instrumenter;
import cdc.util.events.ProgressController;

class AddProbeAdapterTest {
    protected static final Logger LOGGER = LogManager.getLogger(AddProbeAdapterTest.class);

    private static final Configurator CONFIGURATOR = new Configurator() {
        @Override
        public File getBackupFile() {
            return null;
        }

        @Override
        public boolean mustInstrumentClass(String packageName,
                                           String simpleName) {
            LOGGER.debug("mustInstrumentClass(" + packageName + ", " + simpleName + ")");
            return true;
        }

        @Override
        public MeasureLevel getMethodMeasureLevel(String packageName,
                                                  String simpleName,
                                                  String methodName,
                                                  String... methodSignature) {
            LOGGER.debug("getMethodMeasureLevel(" + packageName + ", " + simpleName + ", " + methodName + ", "
                    + Arrays.toString(methodSignature) + ")");
            // return MeasureLevel.INFO;
            if ("m1".equals(methodName)) {
                return MeasureLevel.INFO;
            } else {
                return MeasureLevel.MINOR;
            }
        }

        @Override
        public boolean mustShowMethodArgs(String packageName,
                                          String simpleName,
                                          String methodName,
                                          String... argClassNames) {
            return true;
        }
    };

    private static void instrument(String className) throws Exception {
        LOGGER.debug("instrument(" + className + ")");
        final File input = new File("target/test-classes", className + ".class");
        final File output = new File("target/test-classes", className + "I.class");

        LOGGER.debug("input: " + input);
        LOGGER.debug("output: " + output);

        final Instrumenter instrumenter = new Instrumenter(CONFIGURATOR);

        try (final FileInputStream is = new FileInputStream(input)) {
            final byte[] b = instrumenter.instrument(is, className, className + "I");

            try (final FileOutputStream fos = new FileOutputStream(output)) {
                fos.write(b);
                fos.flush();
            }
            LOGGER.debug("generated " + output);
        }

        final Class<?> cls = Class.forName((className + "I").replaceAll("/", "."));
        final Method m2Method = cls.getMethod("m2", String.class);
        final Constructor<?> ctor = cls.getConstructor();
        final Object object = ctor.newInstance();

        try {
            m2Method.invoke(object, "Hello");
        } catch (final Exception e) {
            e.printStackTrace();
        }

        LOGGER.debug("Sources:");
        for (final Source source : RuntimeEnvironment.getInstance().getSources()) {
            LOGGER.debug("   " + source);
        }

        final XmlWriter writer = new XmlWriter(NonCloseableOutputStream.NON_CLOSABLE_SYSTEM_OUT);
        writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        final PerfsXml.Printer printer = new PerfsXml.Printer();
        printer.printDocument(RuntimeEnvironment.getInstance(), writer, ProgressController.VOID);
    }

    @Test
    void testD1() throws Exception {
        instrument("cdc/test/perfs/instrument/asm/D1");
        assertTrue(true);
    }
}