package cdc.demo.perfs.instrument.asm;

import java.io.IOException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.test.perfs.instrument.asm.D1;

public final class ProbeAgentDemo {
    private static final Logger LOGGER = LogManager.getLogger(ProbeAgentDemo.class);

    private ProbeAgentDemo() {
    }

    public static void main(String[] args) throws IOException {
        LOGGER.debug("main(" + Arrays.toString(args) + ")");

        final D1 d = new D1();
        for (int index = 0; index < 10; index++) {
            d.m1();
            d.m3("Hello", 10.0f);
        }

        // LOGGER.info("Sources:");
        // for (final Source source : RuntimeEnvironment.getInstance().getSources()) {
        // LOGGER.info(" " + source);
        // }

        // final XmlWriter writer = new XmlWriter(NonCloseableOutputStream.NON_CLOSABLE_SYSTEM_OUT);
        // writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        // final PerfsXml.Printer printer = new PerfsXml.Printer();
        // printer.printDocument(RuntimeEnvironment.getInstance(), writer, ProgressController.DEFAULT);
    }
}