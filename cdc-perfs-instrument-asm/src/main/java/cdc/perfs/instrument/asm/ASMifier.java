package cdc.perfs.instrument.asm;

import java.io.IOException;

import cdc.perfs.api.RuntimeManager;
import cdc.util.lang.BlackHole;

public final class ASMifier {

    private static void print(String s) {
        BlackHole.discard(s);
    }

    private void ignore() {
        BlackHole.discard(this);
    }

    // 1 boolean/Boolean

    public static void sz(boolean v) {
        print(RuntimeManager.format("sz", v));
    }

    public static void sZ(Boolean v) {
        print(RuntimeManager.format("sZ", v));
    }

    public void vz(boolean v) {
        print(RuntimeManager.format("vz", v));
        ignore();
    }

    public void vZ(Boolean v) {
        print(RuntimeManager.format("vZ", v));
        ignore();
    }

    // 2 boolean/Boolean

    public static void szz(boolean v1,
                           boolean v2) {
        print(RuntimeManager.format("szz", v1, v2));
    }

    public static void szZ(boolean v1,
                           Boolean v2) {
        print(RuntimeManager.format("szZ", v1, v2));
    }

    public static void sZz(Boolean v1,
                           boolean v2) {
        print(RuntimeManager.format("sZz", v1, v2));
    }

    public static void sZZ(Boolean v1,
                           Boolean v2) {
        print(RuntimeManager.format("sZZ", v1, v2));
    }

    public void vzz(boolean v1,
                    boolean v2) {
        print(RuntimeManager.format("szz", v1, v2));
        ignore();
    }

    public void vzZ(boolean v1,
                    Boolean v2) {
        print(RuntimeManager.format("szZ", v1, v2));
        ignore();
    }

    public void vZz(Boolean v1,
                    boolean v2) {
        print(RuntimeManager.format("sZz", v1, v2));
        ignore();
    }

    public void vZZ(Boolean v1,
                    Boolean v2) {
        print(RuntimeManager.format("sZZ", v1, v2));
        ignore();
    }

    // // 1 char/Character

    public static void sc(char v) {
        print(RuntimeManager.format("sc", v));
    }

    public static void sC(Character v) {
        print(RuntimeManager.format("sC", v));
    }

    public void vc(char v) {
        print(RuntimeManager.format("vc", v));
        ignore();
    }

    public void vC(Character v) {
        print(RuntimeManager.format("vC", v));
        ignore();
    }

    // 1 byte/Byte

    public static void sb(byte v) {
        print(RuntimeManager.format("sb", v));
    }

    public static void sB(Byte v) {
        print(RuntimeManager.format("sB", v));
    }

    public void vb(byte v) {
        print(RuntimeManager.format("vb", v));
        ignore();
    }

    public void vB(Byte v) {
        print(RuntimeManager.format("vB", v));
        ignore();
    }

    // 1 short/Short

    public static void ss(short v) {
        print(RuntimeManager.format("ss", v));
    }

    public static void sS(Short v) {
        print(RuntimeManager.format("sS", v));
    }

    public void vs(short v) {
        print(RuntimeManager.format("vs", v));
        ignore();
    }

    public void vS(Short v) {
        print(RuntimeManager.format("vS", v));
        ignore();
    }

    // 1 int/Integer

    public static void si(int v) {
        print(RuntimeManager.format("si", v));
    }

    public static void sI(Integer v) {
        print(RuntimeManager.format("sI", v));
    }

    public void vi(int v) {
        print(RuntimeManager.format("vi", v));
        ignore();
    }

    public void vI(Integer v) {
        print(RuntimeManager.format("vI", v));
        ignore();
    }

    // 1 long/Long

    public static void sj(long v) {
        print(RuntimeManager.format("sj", v));
    }

    public static void sJ(Long v) {
        print(RuntimeManager.format("sJ", v));
    }

    public void vj(long v) {
        print(RuntimeManager.format("vj", v));
        ignore();
    }

    public void vJ(Long v) {
        print(RuntimeManager.format("vJ", v));
        ignore();
    }

    // 1 float/Float

    public static void sf(float v) {
        print(RuntimeManager.format("sf", v));
    }

    public static void sF(Float v) {
        print(RuntimeManager.format("sF", v));
    }

    public void vf(float v) {
        print(RuntimeManager.format("vf", v));
        ignore();
    }

    public void vF(Float v) {
        print(RuntimeManager.format("vF", v));
        ignore();
    }

    // 1 double/Double

    public static void sd(double v) {
        print(RuntimeManager.format("sd", v));
    }

    public static void sD(Double v) {
        print(RuntimeManager.format("sD", v));
    }

    public void vd(double v) {
        print(RuntimeManager.format("vd", v));
        ignore();
    }

    public void vD(Double v) {
        print(RuntimeManager.format("vD", v));
        ignore();
    }

    // 1 String

    public static void sString(String v) {
        print(RuntimeManager.format("sString", v));
    }

    public void vString(String v) {
        print(RuntimeManager.format("vString", v));
        ignore();
    }

    // Misc

    public void f0() {
        print(RuntimeManager.format("f0"));
        ignore();
    }

    public void f1(String s) {
        print(RuntimeManager.format("f1", s));
        ignore();
    }

    public void f2(String s1,
                   String s2) {
        print(RuntimeManager.format("f2", s1, s2));
        ignore();
    }

    public void f3(String s1,
                   String s2,
                   String s3) {
        print(RuntimeManager.format("f3", s1, s2, s3));
        ignore();
    }

    public void f4(String s1,
                   String s2,
                   String s3,
                   String s4) {
        print(RuntimeManager.format("f4", s1, s2, s3, s4));
        ignore();
    }

    public void f5(String s1,
                   String s2,
                   String s3,
                   String s4,
                   String s5) {
        print(RuntimeManager.format("f5", s1, s2, s3, s4, s5));
        ignore();
    }

    public void f6(String s1,
                   String s2,
                   String s3,
                   String s4,
                   String s5,
                   String s6) {
        print(RuntimeManager.format("f6", s1, s2, s3, s4, s5, s6));
        ignore();
    }

    public void fprim(int s1,
                      float s2,
                      double s3,
                      byte s4,
                      char s5,
                      boolean s6,
                      short s7,
                      long s8) {
        print(RuntimeManager.format("fprim", s1, s2, s3, s4, s5, s6, s7, s8));
        ignore();
    }

    public static void sfprim(int s1,
                              float s2,
                              double s3,
                              byte s4,
                              char s5,
                              boolean s6,
                              short s7,
                              long s8) {
        print(RuntimeManager.format("sfprim", s1, s2, s3, s4, s5, s6, s7, s8));
    }

    public static void main(String[] args) throws IOException {
        org.objectweb.asm.util.ASMifier.main(args);
    }
}