package cdc.perfs.instrument.asm;

import java.io.IOException;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.ClassRemapper;
import org.objectweb.asm.commons.SimpleRemapper;

import cdc.perfs.instrument.Configurator;
import cdc.util.lang.Checks;

public final class Instrumenter {
    private static final Logger LOGGER = LogManager.getLogger(Instrumenter.class);
    private final Configurator configurator;

    public Instrumenter(Configurator configurator) {
        Checks.isNotNull(configurator, "configurator");
        this.configurator = configurator;
    }

    public Configurator getConfigurator() {
        return configurator;
    }

    public byte[] instrument(InputStream is,
                             String inputClassName,
                             String outputClassName) throws IOException {
        LOGGER.debug("instrument(<is>, {}, {})", inputClassName, outputClassName);
        final ClassReader cr = new ClassReader(is);
        final ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);
        final ClassVisitor cv = new ClassRemapper(new AddProbeAdapter(cw, inputClassName, configurator),
                                                  new SimpleRemapper(inputClassName, outputClassName));
        cr.accept(cv, ClassReader.EXPAND_FRAMES);
        return cw.toByteArray();
    }

    public byte[] instrument(byte[] classFile,
                             String inputClassName,
                             String outputClassName) {
        LOGGER.debug("instrument(<classFile>, {}, {})", inputClassName, outputClassName);
        final ClassReader cr = new ClassReader(classFile);
        final ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);
        final ClassVisitor cv = new ClassRemapper(new AddProbeAdapter(cw, inputClassName, configurator),
                                                  new SimpleRemapper(inputClassName, outputClassName));
        cr.accept(cv, ClassReader.EXPAND_FRAMES);
        return cw.toByteArray();
    }
}