package cdc.perfs.instrument.asm;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import cdc.perfs.instrument.Configurator;
import cdc.util.lang.Checks;

public class Transformer implements ClassFileTransformer {
    private final Instrumenter instrumenter;

    // Do this to workaround analysis tools that emit a warning
    private static final byte[] NULL = null;

    public Transformer(Configurator configurator) {
        Checks.isNotNull(configurator, "configurator");
        this.instrumenter = new Instrumenter(configurator);
    }

    @Override
    public byte[] transform(ClassLoader loader,
                            String className,
                            Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        if (instrumenter.getConfigurator().mustInstrumentClass(className)) {
            return instrumenter.instrument(classfileBuffer, className, className);
        } else {
            // null must be returned when no transformation is executed
            return NULL;
        }
    }
}