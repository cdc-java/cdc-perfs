package cdc.perfs.demos;

import java.util.Random;

import javax.swing.SwingUtilities;

import cdc.perfs.ui.RefreshRate;
import cdc.perfs.ui.Rendering;
import cdc.perfs.ui.swing.MainFrame;

public final class SwingPerfsDemo {
    private static final Random RANDOM = new Random();

    private SwingPerfsDemo() {
    }

    static int maxDepth() {
        return 1 + RANDOM.nextInt(3);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            final MainFrame frame = new MainFrame(/* WindowConstants.HIDE_ON_CLOSE */);
            frame.getRuntimePanel().getControlledChartPanel().setDisplayStatsEnabled(true);
            frame.getRuntimePanel().getControlledChartPanel().setRefreshRate(RefreshRate.HIGH);
            frame.getRuntimePanel().getControlledChartPanel().setRendering(Rendering.BEST);
            frame.getRuntimePanel().getControlledChartPanel().setDrawBordersEnabled(true);
            frame.getRuntimePanel().getControlledChartPanel().setScale(100000000.0);

            final boolean randomValues = true;
            final boolean randomErrors = true;
            frame.setVisible(true);
            int index = 0;
            final int delay = 5;
            for (int i = 0; i < 10; i++) {
                PerfsSupport.start(index * delay, -1, 10, 10, 10, false, false);
            }
            index = 0;
            PerfsSupport.start((index++) * delay, 40, 100, 100, 1, false, false);
            PerfsSupport.start((index++) * delay, -1, 1000, 1000, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, -1, 1000, 1000, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
            PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        });
    }
}