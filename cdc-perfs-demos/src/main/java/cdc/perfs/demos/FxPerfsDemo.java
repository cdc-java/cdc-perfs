package cdc.perfs.demos;

import java.util.Random;

import cdc.perfs.ui.RefreshRate;
import cdc.perfs.ui.Rendering;
import cdc.perfs.ui.fx.MainPane;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class FxPerfsDemo extends Application {
    private static final Random RANDOM = new Random();

    protected static int maxDepth() {
        return 1 + RANDOM.nextInt(3);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        final MainPane pane = new MainPane(primaryStage);
        pane.getRuntimePane().getControlledChartPane().setDisplayStatsEnabled(true);
        pane.getRuntimePane().getControlledChartPane().setRefreshRate(RefreshRate.HIGH);
        pane.getRuntimePane().getControlledChartPane().setRendering(Rendering.BEST);
        pane.getRuntimePane().getControlledChartPane().setDrawBordersEnabled(true);
        pane.getRuntimePane().getControlledChartPane().setScale(100000000.0);

        primaryStage.setOnHidden(event -> {
            Platform.exit();
            System.exit(0);
        });

        final boolean randomValues = true;
        final boolean randomErrors = true;
        final int delay = 5000;
        // for (int i = 0; i < 500; i++) {
        // PerfsSupport.start(i * delay, 20, 100, 100, 1, false, false);
        // }

        int index = 0;
        PerfsSupport.start((index++) * delay, 20, 100, 100, 10, false, true);
        PerfsSupport.start((index++) * delay, 40, 100, 100, 10, false, true);
        PerfsSupport.start((index++) * delay, -1, 1000, 1000, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, -1, 1000, 1000, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);
        PerfsSupport.start((index++) * delay, 200, 200, 100, maxDepth(), randomValues, randomErrors);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}