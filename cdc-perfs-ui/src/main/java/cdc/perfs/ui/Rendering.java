package cdc.perfs.ui;

/**
 * Enumeration of possible UI rendering hints.
 *
 * @author Damien Carbonne
 *
 */
public enum Rendering {
    FASTEST("Fastest"),
    BEST("Best");

    private final String label;

    private Rendering(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}