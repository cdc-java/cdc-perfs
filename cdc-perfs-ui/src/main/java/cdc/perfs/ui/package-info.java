/**
 * Shared classes used to implement UI.
 *
 * @author Damien Carbonne
 */
package cdc.perfs.ui;