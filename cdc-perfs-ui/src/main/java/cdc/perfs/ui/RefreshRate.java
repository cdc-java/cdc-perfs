package cdc.perfs.ui;

/**
 * Enumeration of possible UI refresh rates.
 *
 * @author Damien Carbonne
 *
 */
public enum RefreshRate {
    VERY_LOW("Very Low", 500),
    LOW("Low", 100),
    MEDIUM("Medium", 50),
    HIGH("High", 10);

    private String label;
    private int delay;

    private RefreshRate(String label,
                        int delay) {
        this.label = label;
        this.delay = delay;
    }

    public String getLabel() {
        return label;
    }

    public int getDelay() {
        return delay;
    }
}