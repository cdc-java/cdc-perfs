package cdc.perfs.ui;

public enum SourceDisplay {
    HIDDEN("Hidden"),
    NORMAL("Normal"),
    HIGHLIGHTED("Highlighted");

    private final String label;

    private SourceDisplay(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}