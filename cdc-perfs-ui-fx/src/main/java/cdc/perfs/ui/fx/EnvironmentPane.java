package cdc.perfs.ui.fx;

import java.io.File;

import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentKind;
import cdc.perfs.core.io.IoExtension;
import cdc.perfs.core.io.IoMode;
import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.perfs.core.snapshot.SnapshotEnvironment;
import cdc.ui.fx.FxUtils;
import cdc.util.events.ProgressController;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class EnvironmentPane extends BorderPane {
    protected final MainPane wMain;
    private final ToolBar wToolBar = new ToolBar();
    private final Button wSnaphot;
    protected final Button wSave;
    protected final Button wSaveAs;
    private final ContextsTableModel contextsModel;
    private final SourcesTableModel sourcesModel;
    private final ContextsPane wContextsPane;
    private final SourcesPane wSourcesPane;
    private final ControlledChartPane wControlledChartPane;
    private String filename;

    public EnvironmentPane(MainPane main,
                           Environment environment) {
        this.wMain = main;
        setTop(wToolBar);

        if (environment.getKind() == EnvironmentKind.RUNTIME) {
            wSnaphot = new Button("Snapshot");
            wSnaphot.setOnAction(e -> {
                final SnapshotEnvironment snaphot = new SnapshotEnvironment(RuntimeEnvironment.getInstance());
                wMain.addSnaphot(snaphot);
            });

            wSave = null;
            wSaveAs = null;
            wToolBar.getItems().add(wSnaphot);
        } else {
            wSnaphot = null;
            wSave = new Button("Save");
            wSave.setOnAction(e -> save(wSave));
            wToolBar.getItems().add(wSave);

            wSaveAs = new Button("Save As");
            wSaveAs.setOnAction(e -> saveAs(wSaveAs));
            wToolBar.getItems().add(wSaveAs);
        }

        contextsModel = new ContextsTableModel(environment);
        sourcesModel = new SourcesTableModel(environment);

        final SplitPane wSplitPane = new SplitPane();
        wSplitPane.setDividerPosition(0, 0.25);

        setCenter(wSplitPane);

        final TabPane wTabPane = new TabPane();
        wSplitPane.getItems().add(wTabPane);

        {
            wContextsPane = new ContextsPane(contextsModel);
            final Tab wTab = new Tab("Contexts");
            wTab.setClosable(false);
            wTab.setContent(wContextsPane);
            wTabPane.getTabs().add(wTab);
        }

        {
            wSourcesPane = new SourcesPane(sourcesModel);
            final Tab wTab = new Tab("Sources");
            wTab.setClosable(false);
            wTab.setContent(wSourcesPane);
            wTabPane.getTabs().add(wTab);
        }

        wControlledChartPane = new ControlledChartPane(contextsModel);
        wSplitPane.getItems().add(wControlledChartPane);
    }

    public Environment getEnvironment() {
        return wControlledChartPane.getEnvironment();
    }

    public ContextsTableModel getContextsTableModel() {
        return contextsModel;
    }

    public ContextsPane getContextsPane() {
        return wContextsPane;
    }

    public SourcesTableModel getSourcesTableModel() {
        return sourcesModel;
    }

    public SourcesPane getSourcesPane() {
        return wSourcesPane;
    }

    public ControlledChartPane getControlledChartPane() {
        return wControlledChartPane;
    }

    protected void save(Node parent) {
        if (filename == null) {
            saveAs(parent);
        } else {
            saveAs(parent, filename);
        }
    }

    protected void saveAs(Node parent) {
        final FileChooser wChooser = new FileChooser();
        wChooser.setTitle("Open");
        wChooser.getExtensionFilters()
                .addAll(new FileChooser.ExtensionFilter("Snapshot binary or xml files",
                                                        IoExtension.getExtensions("*.", IoMode.EXPORT)));
        final File file = wChooser.showSaveDialog(parent.getScene().getWindow());
        if (file != null) {
            saveAs(null, file.getPath());
        }
    }

    private void saveAs(Node parent,
                        String filename) {
        final String path = IoExtension.fixFilename(filename);
        try {
            PerfsIo.save(getEnvironment(), path, ProgressController.VOID);
            this.filename = path;
        } catch (final Exception e) {
            final Alert alert = new Alert(AlertType.ERROR);
            final Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().addAll(FxUtils.getApplicationImages());
            alert.setTitle("Error");
            alert.setHeaderText("Failed to save '" + path + "'");
            alert.setContentText(e.getClass().getSimpleName() + " " + e.getMessage());
            alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
            alert.showAndWait();
        }
    }
}