package cdc.perfs.ui.fx;

import cdc.perfs.core.Context;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

public class ContextsPane extends BorderPane {
    private final TableView<ContextsTableModel.Record> wTable = new TableView<>();

    public ContextsPane(ContextsTableModel model) {
        wTable.setEditable(true);
        wTable.setItems(model.asObservableList());

        final TableColumn<ContextsTableModel.Record, Context> wContextCol = new TableColumn<>("Context");
        final TableColumn<ContextsTableModel.Record, Boolean> wVisibleCol = new TableColumn<>("Visible");
        final TableColumn<ContextsTableModel.Record, Integer> wMeasuresCol = new TableColumn<>("Measures");

        wTable.getColumns().add(wContextCol);
        wTable.getColumns().add(wVisibleCol);
        wTable.getColumns().add(wMeasuresCol);

        wContextCol.setCellValueFactory(new PropertyValueFactory<>("context"));
        wContextCol.setCellFactory(column -> new ContextTableCell());

        wVisibleCol.setCellValueFactory(f -> f.getValue().visibleProperty());
        wVisibleCol.setEditable(true);
        // wVisibleCol.setCellFactory(CheckBoxTableCell.forTableColumn(wVisibleCol));
        wVisibleCol.setCellFactory(column -> new VisibleTableCell());
        // wVisibleCol.setCellFactory(column -> new BooleanCell<>());

        wVisibleCol.setOnEditCommit(event -> event.getTableView()
                                                  .getItems()
                                                  .get(event.getTablePosition().getRow())
                                                  .setVisible(event.getNewValue()));

        wMeasuresCol.setCellValueFactory(new PropertyValueFactory<>("measures"));
        wMeasuresCol.setCellFactory(column -> new MeasuresTableCell());

        setCenter(wTable);
    }
}