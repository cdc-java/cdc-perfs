package cdc.perfs.ui.fx;

import cdc.perfs.api.Source;
import cdc.perfs.api.SourceLevel;
import cdc.perfs.core.EnvironmentKind;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

public class SourcesPane extends BorderPane {
    private final TableView<SourcesTableModel.Record> wTable = new TableView<>();

    public SourcesPane(SourcesTableModel model) {
        wTable.setEditable(true);
        wTable.setItems(model);

        final TableColumn<SourcesTableModel.Record, Source> wSourceCol = new TableColumn<>("Source");
        final TableColumn<SourcesTableModel.Record, SourceLevel> wMaxLevelCol = new TableColumn<>("Max Level");

        wTable.getColumns().add(wSourceCol);
        wTable.getColumns().add(wMaxLevelCol);

        wSourceCol.setCellValueFactory(new PropertyValueFactory<>("source"));

        wMaxLevelCol.setCellValueFactory(new PropertyValueFactory<>("maxLevel"));
        // wMaxLevelCol.setCellFactory(ComboBoxTableCell.forTableColumn(MeasureLevel.values()));
        wMaxLevelCol.setCellFactory(column -> new SourceLevelTableCell());
        wMaxLevelCol.setEditable(model.getEnvironment().getKind() == EnvironmentKind.RUNTIME);
        wMaxLevelCol.setOnEditCommit(e -> e.getTableView().getItems().get(e.getTablePosition().getRow())
                                           .setMaxLevel(e.getNewValue()));

        setCenter(wTable);
    }
}