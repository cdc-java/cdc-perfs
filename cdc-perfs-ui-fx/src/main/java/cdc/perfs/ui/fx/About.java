package cdc.perfs.ui.fx;

import cdc.ui.fx.FxUtils;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class About extends Stage {
    public About() {
        getIcons().addAll(FxUtils.getApplicationImages());
        setTitle("About CDC Perfs Monitor");
        setResizable(false);

        final TextFlow wTextFlow = new TextFlow();
        wTextFlow.setLineSpacing(5.0);
        wTextFlow.setPadding(new Insets(5.0));

        wTextFlow.getChildren().add(FxUtils.toBoldText("CDC Perfs Monitor"));
        wTextFlow.getChildren().add(FxUtils.toBoldText("\nVersion: "));
        wTextFlow.getChildren().add(FxUtils.toText(cdc.perfs.api.Config.VERSION + " (JavaFx)"));
        wTextFlow.getChildren().add(FxUtils.toText("\n\n(c) Copyright 2010-2024, Damien Carbonne"));

        final Scene scene = new Scene(wTextFlow);
        setScene(scene);
        show();
    }
}