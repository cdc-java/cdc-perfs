package cdc.perfs.ui.fx;

import cdc.perfs.core.Context;
import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;

public class ContextTableCell extends TableCell<ContextsTableModel.Record, Context> {
    @Override
    protected void updateItem(Context item,
                              boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setText(null);
        } else {
            setText(item.getName());
            if (item.isAlive()) {
                setTextFill(Color.BLACK);
            } else {
                setTextFill(Color.RED);
            }
        }
    }
}