package cdc.perfs.ui.fx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cdc.perfs.core.Context;
import cdc.perfs.core.io.IoExtension;
import cdc.perfs.core.io.IoMode;
import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.perfs.core.snapshot.SnapshotEnvironment;
import cdc.perfs.ui.RefreshRate;
import cdc.perfs.ui.Rendering;
import cdc.ui.fx.FxUtils;
import cdc.util.files.Files;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MainPane extends BorderPane {
    protected final TabPane wTabPane = new TabPane();
    private final EnvironmentPane wRuntimePane;
    private final List<EnvironmentPane> wEnvironmentPanes = new ArrayList<>();
    private About wAbout = null;
    /** Directory to use for Open File dialog. */
    protected File openDir = Files.currentDir();

    public MainPane(Stage stage) {
        stage.getIcons().addAll(FxUtils.getApplicationImages());
        stage.setTitle("CDC Perfs Monitor");
        stage.setWidth(800.0);
        stage.setHeight(500.0);

        setCenter(wTabPane);
        wRuntimePane = new EnvironmentPane(this, RuntimeEnvironment.getInstance());
        wEnvironmentPanes.add(wRuntimePane);
        final Tab wTab = new Tab("Runtime");
        wTab.setContent(wRuntimePane);
        wTab.setClosable(false);
        wTabPane.getTabs().add(wTab);

        final Scene scene = new Scene(this);
        stage.setScene(scene);
        buildMenus();
    }

    public EnvironmentPane getRuntimePane() {
        return wRuntimePane;
    }

    public List<EnvironmentPane> getEnvironmentPanes() {
        return wEnvironmentPanes;
    }

    void addSnaphot(SnapshotEnvironment environment) {
        final EnvironmentPane wSnapshotPane = new EnvironmentPane(this, environment);
        final Tab wTab = new Tab("Snapshot");
        wTab.setContent(wSnapshotPane);
        wTabPane.getTabs().add(wTab);

        // Configure the newly created environment panel to match
        // runtime panel configuration

        final ControlledChartPane rtDisplay = wRuntimePane.getControlledChartPane();
        final ControlledChartPane ssDisplay = wSnapshotPane.getControlledChartPane();
        ssDisplay.setDisplayStatsEnabled(rtDisplay.getDisplayStatsEnabled());
        ssDisplay.setRefreshRate(rtDisplay.getRefreshRate());
        ssDisplay.setRendering(rtDisplay.getRendering());
        ssDisplay.setDrawBordersEnabled(rtDisplay.getDrawBordersEnabled());
        ssDisplay.setScale(rtDisplay.getScale());
        ssDisplay.setFocusNanos(rtDisplay.getFocusNanos());

        // Reproduce contexts visibility
        final ContextsTableModel rtModel = wRuntimePane.getContextsTableModel();
        final ContextsTableModel ssModel = wSnapshotPane.getContextsTableModel();
        for (final Context rtContext : rtModel.getContexts()) {
            final Context ssContext = ssModel.getContext(rtContext.getId());
            if (ssContext != null) {
                final boolean visible = rtModel.isVisible(rtContext);
                ssModel.setVisible(ssContext, visible);
            }
        }

        wEnvironmentPanes.add(wSnapshotPane);
    }

    private void buildMenus() {
        final MenuBar wMenuBar = new MenuBar();
        setTop(wMenuBar);
        buildFileMenu(wMenuBar);
        buildSettingsMenu(wMenuBar);
        buildHelpMenu(wMenuBar);
    }

    private void buildFileMenu(MenuBar wMenuBar) {
        final Menu wMenu = new Menu("File");
        wMenuBar.getMenus().add(wMenu);
        FxUtils.addMenuItem(wMenu, "Open", e -> open(wTabPane));
    }

    private void buildSettingsMenu(MenuBar wMenuBar) {
        final Menu wMenu = new Menu("Settings");

        // Refresh items when this menu is shown
        wMenu.setOnShowing(event -> {
            // Warning : this code must be compliant with order of
            // declarations of menus
            final Menu source = (Menu) event.getSource();
            refreshRefreshRate(source, 0);
            refreshRendering(source, 1);
            refreshDrawBorders(source, 2);
            refreshShowStats(source, 3);
        });

        wMenuBar.getMenus().add(wMenu);
        buildRefreshRateMenu(wMenu);
        buildRenderingMenu(wMenu);
        buildDrawBordersMenuItem(wMenu);
        buildShowStatsMenuItem(wMenu);
    }

    void refreshRefreshRate(Menu source,
                            int pos) {
        final RefreshRate rate = getRuntimePane().getControlledChartPane().getRefreshRate();
        final Menu menu = (Menu) source.getItems().get(pos);
        for (int index = 0; index < menu.getItems().size(); index++) {
            final RadioMenuItem item = (RadioMenuItem) menu.getItems().get(index);
            if (item.getUserData().equals(rate.name())) {
                item.setSelected(true);
            }
        }
    }

    void refreshRendering(Menu source,
                          int pos) {
        final Rendering rendering = getRuntimePane().getControlledChartPane().getRendering();
        final Menu menu = (Menu) source.getItems().get(pos);
        for (int index = 0; index < menu.getItems().size(); index++) {
            final RadioMenuItem item = (RadioMenuItem) menu.getItems().get(index);
            if (item.getUserData().equals(rendering.name())) {
                item.setSelected(true);
            }
        }
    }

    void refreshDrawBorders(Menu source,
                            int pos) {
        final boolean enabled = getRuntimePane().getControlledChartPane().getDrawBordersEnabled();
        final CheckMenuItem item = (CheckMenuItem) source.getItems().get(pos);
        item.setSelected(enabled);
    }

    void refreshShowStats(Menu source,
                          int pos) {
        final boolean enabled = getRuntimePane().getControlledChartPane().getDisplayStatsEnabled();
        final CheckMenuItem item = (CheckMenuItem) source.getItems().get(pos);
        item.setSelected(enabled);
    }

    private void buildRefreshRateMenu(Menu wMenu) {
        final Menu wRefreshRate = new Menu("Refresh rate");
        wMenu.getItems().add(wRefreshRate);
        final ToggleGroup group = new ToggleGroup();
        for (final RefreshRate rate : RefreshRate.values()) {
            final RadioMenuItem wMenuItem =
                    new RadioMenuItem(rate.getLabel() + " (" + rate.getDelay() + "ms)");
            wMenuItem.setUserData(rate.name());
            wRefreshRate.getItems().add(wMenuItem);
            if (getRuntimePane().getControlledChartPane().getRefreshRate() == rate) {
                wMenuItem.setSelected(true);
            }
            group.getToggles().add(wMenuItem);
            wMenuItem.setOnAction(event -> {
                final String cmd = (String) ((RadioMenuItem) event.getSource()).getUserData();
                final RefreshRate lrate = RefreshRate.valueOf(cmd);
                for (final EnvironmentPane pane : getEnvironmentPanes()) {
                    pane.getControlledChartPane().setRefreshRate(lrate);
                }
            });
        }
    }

    private void buildRenderingMenu(Menu wMenu) {
        final Menu wRendering = new Menu("Rendering");
        wMenu.getItems().add(wRendering);
        final ToggleGroup group = new ToggleGroup();
        for (final Rendering rendering : Rendering.values()) {
            final RadioMenuItem wMenuItem = new RadioMenuItem(rendering.getLabel());
            wMenuItem.setUserData(rendering.name());
            wRendering.getItems().add(wMenuItem);
            if (getRuntimePane().getControlledChartPane().getRendering() == rendering) {
                wMenuItem.setSelected(true);
            }
            group.getToggles().add(wMenuItem);
            wMenuItem.setOnAction(event -> {
                final String cmd = (String) ((RadioMenuItem) event.getSource()).getUserData();
                final Rendering lrendering = Rendering.valueOf(cmd);
                for (final EnvironmentPane pane : getEnvironmentPanes()) {
                    pane.getControlledChartPane().setRendering(lrendering);
                }
            });
        }
    }

    private void buildDrawBordersMenuItem(Menu wMenu) {
        final CheckMenuItem wMenuItem = new CheckMenuItem("Draw borders");
        wMenu.getItems().add(wMenuItem);
        wMenuItem.setSelected(getRuntimePane().getControlledChartPane().getDrawBordersEnabled());
        wMenuItem.setOnAction(event -> {
            final CheckMenuItem source = (CheckMenuItem) event.getSource();
            for (final EnvironmentPane pane : getEnvironmentPanes()) {
                pane.getControlledChartPane().setDrawBordersEnabled(source.isSelected());
            }
        });
    }

    private void buildShowStatsMenuItem(Menu wMenu) {
        final CheckMenuItem wMenuItem = new CheckMenuItem("Show stats");
        wMenu.getItems().add(wMenuItem);
        wMenuItem.setSelected(getRuntimePane().getControlledChartPane().getDrawBordersEnabled());
        wMenuItem.setOnAction(event -> {
            final CheckMenuItem source = (CheckMenuItem) event.getSource();
            for (final EnvironmentPane panel : getEnvironmentPanes()) {
                panel.getControlledChartPane().setDisplayStatsEnabled(source.isSelected());
            }
        });
    }

    private void buildHelpMenu(MenuBar wMenuBar) {
        final Menu wMenu = new Menu("Help");
        wMenuBar.getMenus().add(wMenu);
        FxUtils.addMenuItem(wMenu, "About", e -> showAbout());
    }

    protected void showAbout() {
        if (wAbout == null) {
            wAbout = new About();
        }
        wAbout.show();
        wAbout.toFront();
    }

    protected void open(Node parent) {
        final FileChooser wChooser = new FileChooser();
        wChooser.setInitialDirectory(openDir);
        wChooser.setTitle("Open");
        wChooser.getExtensionFilters()
                .addAll(new FileChooser.ExtensionFilter("Snapshot binary or xml files",
                                                        IoExtension.getExtensions("*.", IoMode.IMPORT)));
        final File file = wChooser.showOpenDialog(parent.getScene().getWindow());
        if (file != null) {
            openDir = file.getParentFile();
            try {
                final SnapshotEnvironment environment = PerfsIo.load(file);
                addSnaphot(environment);
                // TODO filename
            } catch (final Exception e) {
                final Alert alert = new Alert(AlertType.ERROR);
                final Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                stage.getIcons().addAll(FxUtils.getApplicationImages());
                alert.setTitle("Error");
                alert.setHeaderText("Failed to open '" + file.getPath() + "'");
                alert.setContentText(e.getClass().getSimpleName() + " " + e.getMessage());
                alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                alert.showAndWait();
            }
        }
    }
}