package cdc.perfs.ui.fx;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import cdc.perfs.api.Source;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentListener;
import cdc.perfs.core.Measure;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

final class ContextsTableModel {
    private final Environment environment;
    private final List<Context> contexts = new CopyOnWriteArrayList<>();
    private final ObservableList<Record> delegate = FXCollections.observableArrayList(record -> new Observable[] {
            record.contextProperty(),
            record.visibleProperty()
    });

    private final EnvironmentListener listener = new EnvironmentListener() {
        @Override
        public void processContextCreated(Context context) {
            createRecord(context);
        }

        @Override
        public void processContextChanged(Context context) {
            updateRecord(context);
        }

        @Override
        public void processSourceCreated(Source source) {
            // Ignore
        }

        @Override
        public void processSourceChanged(Source source) {
            // Ignore
        }

        @Override
        public void processMeasureCreated(Context context,
                                          Measure measure) {
            // Ignore
        }

        @Override
        public void processMeasureChanged(Measure measure) {
            // Ignore
        }
    };

    public ContextsTableModel(Environment environment) {
        this.environment = environment;
        for (final Context context : environment.getContexts()) {
            createRecord(context);
        }
        environment.addListener(listener);
    }

    public static final class Record {
        private final ObjectProperty<Context> context = new SimpleObjectProperty<>(null);
        private final SimpleBooleanProperty visible = new SimpleBooleanProperty(true);
        private final SimpleIntegerProperty measures = new SimpleIntegerProperty(0);

        public Record(Context context,
                      boolean visible) {
            this.context.set(context);
            this.visible.set(visible);
            this.measures.set(context.getMeasuresCount());
        }

        public Context getContext() {
            return context.get();
        }

        public ObjectProperty<Context> contextProperty() {
            return context;
        }

        public boolean isVisible() {
            return visible.get();
        }

        public void setVisible(boolean value) {
            visible.set(value);
        }

        public SimpleBooleanProperty visibleProperty() {
            return visible;
        }

        public void setMeasures(int value) {
            measures.set(value);
        }

        public SimpleIntegerProperty measuresProperty() {
            return measures;
        }
    }

    protected synchronized void createRecord(Context context) {
        final Record record = new Record(context, true);
        contexts.add(context);
        delegate.addAll(record);
        record.visibleProperty()
              .addListener((observable,
                            oldValue,
                            newValue) -> updateRecord(context));
        record.measuresProperty()
              .addListener((observable,
                            oldValue,
                            newValue) -> updateRecord(context));
    }

    protected synchronized void updateRecord(Context context) {
        final Record record = getRecord(context);
        if (record != null) {
            record.contextProperty().set(null);
            record.contextProperty().set(context);
            record.measuresProperty().set(context.getMeasuresCount());
        }
    }

    protected int getIndex(Context context) {
        return contexts.indexOf(context);
    }

    private Record getRecord(Context context) {
        final int index = getIndex(context);
        return index < delegate.size() ? delegate.get(index) : null;
    }

    public ObservableList<Record> asObservableList() {
        return delegate;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public List<Context> getContexts() {
        return contexts;
    }

    public Context getContext(int id) {
        for (final Context context : contexts) {
            if (context.getId() == id) {
                return context;
            }
        }
        return null;
    }

    public boolean isVisible(Context context) {
        final Record record = getRecord(context);
        return record != null && record.isVisible();
    }

    public void setVisible(Context context,
                           boolean visible) {
        final Record record = getRecord(context);
        if (record != null) {
            record.setVisible(visible);
        }
    }
}