package cdc.perfs.ui.fx;

import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentKind;
import cdc.perfs.ui.PerfsChartHelper;
import cdc.perfs.ui.RefreshRate;
import cdc.perfs.ui.Rendering;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Slider;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.util.Duration;

public class ControlledChartPane extends BorderPane {
    protected final ScrollPane wScrollPane = new ScrollPane();
    protected final Button wMoveToBegin = new Button("|<");
    protected final Button wLess = new Button("<");
    protected final Button wMore = new Button(">");
    protected final Button wMoveToEnd = new Button(">|");
    private final CheckBox wTimeLocked;
    protected final Slider wScale = new Slider();
    protected final ChartPane wChart;

    private enum TimeChangeMode {
        DEC,
        INC,
        NONE
    }

    protected final Timeline timeline;
    protected TimeChangeMode timeChangeMode = TimeChangeMode.NONE;
    protected long timeChangeInit;
    private final PerfsChartHelper.ChangeListener changeHandler = source -> refresh();

    private final EventHandler<ActionEvent> timeChangeHandler;

    public ControlledChartPane(ContextsTableModel contextsModel) {
        wChart = new ChartPane(contextsModel);

        timeChangeHandler = event -> {
            final long nanos = System.nanoTime() - timeChangeInit;
            switch (timeChangeMode) {
            case DEC:
                wChart.decrementFocus(nanos);
                break;
            case INC:
                wChart.incrementFocus(nanos);
                break;
            default:
                break;
            }
        };
        timeline = new Timeline();
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(10.0), timeChangeHandler));

        wChart.addChangeListener(this.changeHandler);
        wScrollPane.setContent(wChart);
        wScrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        wScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
        setCenter(wScrollPane);

        wScrollPane.viewportBoundsProperty().addListener((observable,
                                                          oldValue,
                                                          newValue) -> computeBounds());
        wScrollPane.hvalueProperty().addListener((observable,
                                                  oldValue,
                                                  newValue) -> computeBounds());
        wScrollPane.vvalueProperty().addListener((observable,
                                                  oldValue,
                                                  newValue) -> computeBounds());

        final ToolBar wToolBar = new ToolBar();

        // Move to begin
        wMoveToBegin.setDisable(!wChart.isFocusLocked());
        wMoveToBegin.setOnAction(event -> wChart.setFocusNanos(0.0));
        wToolBar.getItems().add(wMoveToBegin);

        // Less
        wLess.setDisable(!wChart.isFocusLocked());
        wLess.setOnMousePressed(evenet -> {
            timeChangeMode = TimeChangeMode.DEC;
            timeChangeInit = System.nanoTime();
            timeline.play();
        });
        wLess.setOnMouseReleased(event -> {
            timeline.stop();
            timeChangeMode = TimeChangeMode.NONE;
        });
        wToolBar.getItems().add(wLess);

        // Spacer
        {
            final Pane spacer = new Pane();
            HBox.setHgrow(spacer, Priority.SOMETIMES);
            wToolBar.getItems().add(spacer);
        }

        // Time lock
        wTimeLocked = new CheckBox("Locked");
        if (getEnvironment().getKind() == EnvironmentKind.SNAPSHOT) {
            wTimeLocked.setDisable(true);
            wTimeLocked.setSelected(true);
            wChart.setFocusLocked(true);
            wLess.setDisable(!wChart.isFocusLocked());
            wMoveToBegin.setDisable(!wChart.isFocusLocked());
            wMoveToEnd.setDisable(!wChart.isFocusLocked());
            wMore.setDisable(!wChart.isFocusLocked());
        } else {
            wTimeLocked.setOnAction(event -> {
                final CheckBox source = (CheckBox) event.getSource();
                wChart.setFocusLocked(source.isSelected());
                wLess.setDisable(!wChart.isFocusLocked());
                wMoveToBegin.setDisable(!wChart.isFocusLocked());
                wMoveToEnd.setDisable(!wChart.isFocusLocked());
                wMore.setDisable(!wChart.isFocusLocked());
            });
        }
        wToolBar.getItems().add(wTimeLocked);

        // Scale
        wScale.setShowTickMarks(true);
        wScale.setMajorTickUnit(PerfsChartHelper.SCALE_MAX / 5.0);
        wScale.setMinorTickCount(2);
        wScale.setMin(0.0);
        wScale.setMax(PerfsChartHelper.SCALE_MAX);
        wScale.setValue(PerfsChartHelper.SCALE_MAX / 2.0);
        wScale.valueProperty().addListener((observable,
                                            oldValue,
                                            newValue) -> {
            final double scale = PerfsChartHelper.sliderToScale(wScale.getValue());
            wChart.setScale(scale);
        });
        wToolBar.getItems().add(wScale);

        // Spacer
        {
            final Pane spacer = new Pane();
            HBox.setHgrow(spacer, Priority.SOMETIMES);
            wToolBar.getItems().add(spacer);
        }

        // More
        wMore.setDisable(!wChart.isFocusLocked());
        wMore.setOnMousePressed(event -> {
            timeChangeMode = TimeChangeMode.INC;
            timeChangeInit = System.nanoTime();
            timeline.play();
        });
        wMore.setOnMouseReleased(event -> {
            timeline.stop();
            timeChangeMode = TimeChangeMode.NONE;
        });
        wToolBar.getItems().add(wMore);

        // Move to end
        wMoveToEnd.setDisable(!wChart.isFocusLocked());
        wMoveToEnd.setOnAction(event -> wChart.setFocusNanos(wChart.getEnvironment().getElapsedNanos()));
        wToolBar.getItems().add(wMoveToEnd);

        setBottom(wToolBar);
    }

    private void computeBounds() {
        final double vValue = wScrollPane.getVvalue();
        final double width = wScrollPane.viewportBoundsProperty().get().getWidth();
        final double height = wScrollPane.viewportBoundsProperty().get().getHeight();

        final double y = Math.max(0.0, (wScrollPane.getContent().getBoundsInParent().getHeight() - height) * vValue);

        wChart.setViewportBounds(width, height, y);
    }

    public Environment getEnvironment() {
        return wChart.getEnvironment();
    }

    public void setRefreshRate(RefreshRate rate) {
        wChart.setRefreshRate(rate);
    }

    public RefreshRate getRefreshRate() {
        return wChart.getRefreshRate();
    }

    public void setRendering(Rendering rendering) {
        wChart.setRendering(rendering);
    }

    public Rendering getRendering() {
        return wChart.getRendering();
    }

    public void setDisplayStatsEnabled(boolean enabled) {
        wChart.setDisplayStatsEnabled(enabled);
    }

    public boolean getDisplayStatsEnabled() {
        return wChart.getDisplayStatsEnabled();
    }

    public void setDrawBordersEnabled(boolean enabled) {
        wChart.setDrawBordersEnabled(enabled);
    }

    public boolean getDrawBordersEnabled() {
        return wChart.getDrawBordersEnabled();
    }

    public void setScale(double scale) {
        wChart.setScale(scale);
    }

    public double getScale() {
        return wChart.getScale();
    }

    public void setFocusNanos(double time) {
        wChart.setFocusNanos(time);
    }

    public double getFocusNanos() {
        return wChart.getFocusNanos();
    }

    protected void refresh() {
        wScale.setValue(PerfsChartHelper.scaleToSlider(wChart.getScale()));
    }
}