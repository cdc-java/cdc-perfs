package cdc.perfs.ui.fx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.ui.fx.FxUtils;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class VisibleTableCell extends TableCell<ContextsTableModel.Record, Boolean> {
    private static final Logger LOGGER = LogManager.getLogger(VisibleTableCell.class);
    private static Image onImage = null;
    private final ImageView onImageView;
    protected boolean value = false;

    public VisibleTableCell() {
        init();
        onImageView = new ImageView(onImage);
    }

    private static synchronized void init() {
        if (onImage == null) {
            onImage = FxUtils.load("cdc/perfs/images/cdc-perfs-visible.png");
        }
    }

    @Override
    public void updateItem(Boolean item,
                           boolean empty) {
        LOGGER.debug("updateItem(" + item + ", " + empty + ")");
        super.updateItem(item, empty);

        if (empty || item == null) {
            setGraphic(null);
            setText(null);
        } else {
            setAlignment(Pos.CENTER);
            value = item;
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            if (item) {
                setGraphic(onImageView);
            } else {
                setGraphic(null);
            }
            setOnMouseClicked(e -> {
                value = !value;
                commitEdit(value);
            });
        }
    }
}