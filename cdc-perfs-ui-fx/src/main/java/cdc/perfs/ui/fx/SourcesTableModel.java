package cdc.perfs.ui.fx;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import cdc.perfs.api.Source;
import cdc.perfs.api.SourceLevel;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentListener;
import cdc.perfs.core.Measure;
import javafx.application.Platform;
import javafx.collections.ModifiableObservableListBase;

public final class SourcesTableModel extends ModifiableObservableListBase<SourcesTableModel.Record> {
    private final Environment environment;
    private final List<Record> delegate = new CopyOnWriteArrayList<>();

    private final EnvironmentListener listener = new EnvironmentListener() {
        @Override
        public void processContextCreated(Context context) {
            // Ignore
        }

        @Override
        public void processContextChanged(Context context) {
            // Ignore
        }

        @Override
        public void processSourceCreated(Source source) {
            createRecord(source);
        }

        @Override
        public void processSourceChanged(Source source) {
            updateRecord(source);
        }

        @Override
        public void processMeasureCreated(Context context,
                                          Measure measure) {
            // Ignore
        }

        @Override
        public void processMeasureChanged(Measure measure) {
            // Ignore
        }
    };

    public SourcesTableModel(Environment environment) {
        this.environment = environment;
        for (final Source source : environment.getSources()) {
            createRecord(source);
        }
        environment.addListener(listener);
    }

    public static class Record {
        private final Source source;

        public Record(Source source) {
            this.source = source;
        }

        public Source getSource() {
            return source;
        }

        public SourceLevel getMaxLevel() {
            return source.getMaxLevel();
        }

        public void setMaxLevel(SourceLevel level) {
            source.setMaxLevel(level);
        }
    }

    synchronized void createRecord(Source source) {
        final Record rec = new Record(source);
        Platform.runLater(() -> addAll(rec));
    }

    private int getIndex(Source source) {
        // Not efficient, but should be OK (?)
        for (int index = 0; index < size(); index++) {
            if (delegate.get(index).getSource() == source) {
                return index;
            }
        }
        return -1;
    }

    synchronized void updateRecord(Source source) {
        final int index = getIndex(source);
        beginChange();
        nextUpdate(index);
        endChange();
    }

    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public Record get(int index) {
        return delegate.get(index);
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    protected void doAdd(int index,
                         Record element) {
        delegate.add(index, element);
    }

    @Override
    protected Record doSet(int index,
                           Record element) {
        return delegate.set(index, element);
    }

    @Override
    protected Record doRemove(int index) {
        return delegate.remove(index);
    }
}