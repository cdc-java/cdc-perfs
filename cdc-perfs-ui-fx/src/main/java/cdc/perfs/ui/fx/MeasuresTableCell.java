package cdc.perfs.ui.fx;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;

public class MeasuresTableCell extends TableCell<ContextsTableModel.Record, Integer> {
    @Override
    protected void updateItem(Integer item,
                              boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setText(null);
        } else {
            setText(String.format("%,d", item));
            setAlignment(Pos.CENTER_RIGHT);
        }
    }
}