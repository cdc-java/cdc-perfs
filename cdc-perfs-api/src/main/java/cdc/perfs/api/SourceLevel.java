package cdc.perfs.api;

import cdc.util.encoding.Encoded;
import cdc.util.encoding.Encoder;
import cdc.util.encoding.Encoders;

/**
 * Enumeration of possible source levels.
 * <p>
 * They are enumerated from most important (less verbose) to less important
 * (more verbose).
 *
 * @author Damien Carbonne
 */
public enum SourceLevel implements Encoded<Character> {
    /**
     * Most important level (less verbose level).
     */
    NONE("None", 'N'),
    INFO(MeasureLevel.INFO),
    MAJOR(MeasureLevel.MAJOR),
    MINOR(MeasureLevel.MINOR),
    MICRO(MeasureLevel.MICRO),

    /**
     * Less important level (more verbose)
     */
    DEBUG(MeasureLevel.DEBUG);

    private final MeasureLevel ref;
    private final String label;
    private final Character code;

    private SourceLevel(MeasureLevel ref) {
        this.ref = ref;
        this.label = ref.getLabel();
        this.code = ref.getCode();
    }

    private SourceLevel(String label,
                        char code) {
        this.ref = null;
        this.label = label;
        this.code = code;
    }

    public static final Encoder<SourceLevel, Character> ENCODER = Encoders.encoder(SourceLevel.class, Character.class);

    public String getLabel() {
        return label;
    }

    @Override
    public Character getCode() {
        return code;
    }

    public MeasureLevel toMeasureLevel() {
        return ref;
    }
}