package cdc.perfs.api;

import cdc.util.encoding.Encoded;
import cdc.util.encoding.Encoder;
import cdc.util.encoding.Encoders;

/**
 * Enumeration of possible measure levels.
 * <p>
 * They are enumerated from most important (less verbose) to less important
 * (more verbose).<br>
 * The NONE value can only be used for setting max level of a source.
 *
 * @author Damien Carbonne
 */
public enum MeasureLevel implements Encoded<Character> {
    /**
     * Most important level (less verbose level).
     * This value <em>SHOULD NOT</em> be used for measures, only for source max level.
     */
    NONE("None", 'N'),
    INFO("Info", 'I'),
    MAJOR("Major", 'M'),
    MINOR("Minor", 'm'),
    MICRO("Micro", 'u'),

    /**
     * Less important level (more verbose)
     */
    DEBUG("Debug", 'D');

    private final String label;
    private final Character code;

    private MeasureLevel(String label,
                         char code) {
        this.label = label;
        this.code = code;
    }

    public static final Encoder<MeasureLevel, Character> ENCODER = Encoders.encoder(MeasureLevel.class, Character.class);

    public String getLabel() {
        return label;
    }

    /**
     * @return {@code true} if this level is valid for a measure.
     */
    public boolean isValidForMeasure() {
        return this != NONE;
    }

    @Override
    public Character getCode() {
        return code;
    }
}