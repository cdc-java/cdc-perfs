package cdc.perfs.api;

/**
 * Used to create {@code RuntimeMeasure}s at a certain level.
 * <p>
 * A RuntimeProbe can be created from the {@code RuntimeEnvironment}.
 *
 * @author Damien Carbonne
 */
public interface RuntimeProbe {
    /**
     * @return The associated source.
     */
    public Source getSource();

    /**
     * @return The level of generated measures.
     */
    public MeasureLevel getLevel();

    /**
     * Returns whether this probe is enabled or not.
     * <p>
     * A measure will be effectively done only when enabled.
     *
     * @return Whether this probe is enabled or not.
     */
    public boolean isEnabled();

    /**
     * Starts a new measure corresponding to a detail.
     *
     * @param details Details associated to the measure.
     */
    public void start(String details);

    /**
     * Starts a new measure with no (null) detail.
     */
    public void start();

    /**
     * Stops the current measure, if any, and start a new one with a given
     * detail.
     * <p>
     * If no current measure is running, an error measure will be generated.
     *
     * @param details Details associated to the new measure.
     */
    public void restart(String details);

    /**
     * Stops the current measure, if any, and start a new one with no detail.
     */
    public void restart();

    /**
     * Stops the current measure, if any.
     * <p>
     * Otherwise, creates a measure indicating the error.
     */
    public void stop();
}