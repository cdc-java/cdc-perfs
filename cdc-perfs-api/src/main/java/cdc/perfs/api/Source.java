package cdc.perfs.api;

/**
 * Source of Measures.
 * <p>
 * It can be configured to filter out measures that have an insufficient level.
 * They are created in a Environment and are specific to an Environment.
 * The same name can be used in several environments.
 *
 * @author Damien Carbonne
 */
public interface Source {

    /**
     * Returns the name of this source.
     * <p>
     * In a given environment, only one source can have a name.
     *
     * @return The name of this source.
     */
    public String getName();

    /**
     * Sets the maximum level of allowed measures for this source.
     *
     * @param maxLevel Maximum level of allowed measures.
     */
    public void setMaxLevel(SourceLevel maxLevel);

    /**
     * Returns the finer level of measures that are generated for this source.
     *
     * @return The maximum level of this source.
     */
    public SourceLevel getMaxLevel();

    /**
     * Returns whether a level is allowed for this source.
     *
     * @param level The interrogated level.
     * @return True when level is allowed for this source.
     */
    public boolean isEnabled(MeasureLevel level);

    /**
     * @return The number of measures associated to this Source.
     */
    public int getMeasuresCount();
}