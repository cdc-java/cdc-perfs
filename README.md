Library dedicated to measure of performances

It contains several modules:
- **Core module**
- **Instrument module**
- **UI module**
- **UI Swing module**
- **UI FX module**

# Core
This allows:
- creation of runtime measures.
- creation of snapshots.

Snapshots can be saved and restored (XML and binary formats, possibly compressed).
CSV export is also supported, but not import. 

# Instrument
This module contains an agent that can be used:
- to instrument code with probes,
- and optionally save measures in the end.

For a package, instrumentation can be enabled:
- for all methods of all classes of the package.
- for all methods of some classes of the package.
- for some methods of some classes of the package.   

An xml config file is structured as follows: 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<perfs-config>
   <!-- To save perfs in the end -->
   <backup>backup file name</backup>
   
   <!-- To instrument all methods of all classes of a package -->
   <package name="package name">
      <all-classes level="measure level"/>
   </package>
   ...

   <!-- To instrument all methods of a class of a package -->
   <package name="package name">
      <class name="class name">
         <all-methods level="measure level"/>
      </class>
      ...
   </package>
   ...
   
   <!-- To instrument all methods with a particular name of a class of a package -->
   <package name="package name">
      <class name="class name">
         <method name="method name" level="measure level">
            <all-args/>
         </method>
         ...
      </class>
      ...
   </package>
   ...

   <!-- To instrument the method with a particular name and signature of a class of a package -->
   <package name="package name">
      <class name="class name">
         <method name="method name" level="measure level">
            <arg type="type name"/>
            ...
         </method>
         ...
      </class>
      ...
   </package>
   ...  
</perfs-config>
```

`package name` is written as:
- p1/p2/...
- p1.p2.... (?)

`class name` is written as:
- C
- C1$C2$... (?)

`measure level` is one of:
- INFO
- MAJOR
- MINOR
- MICRO
- DEBUG

To enable schema validation, replace root element `<perfs-config>` with:

```xml
<perfs-config xmlns="https://www.gitlab.com/cdc-java"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="https://www.gitlab.com/cdc-java https://www.gitlab.com/cdc-java/perfs-config.xsd">
```

To enable instrumentation, add this to VM args: `-javaagent:{path-to-jar}/cdc-perfs-instrument-{version}.jar={path-to-config-file.xml}`  

# UI
It contains code shared by other UI modules.

# UI Swing
Graphical display of measures using Swing.
This is the most advanced user interface.

![Image](cdc-perfs-ui-swing/screenshots/runtime-contexts.png)

![Image](cdc-perfs-ui-swing/screenshots/runtime-sources.png)

![Image](cdc-perfs-ui-swing/screenshots/snapshot-contexts.png)


# UI FX 
Graphical display of measures using JavaFX.
This interface has less functionalities than the Swing one (2019/10).  

![Image](cdc-perfs-ui-fx/screenshots/runtime-contexts.png)