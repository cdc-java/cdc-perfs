package cdc.perfs.ui.swing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.table.AbstractTableModel;

import cdc.perfs.api.Source;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentListener;
import cdc.perfs.core.Measure;

/**
 * Implementation of a table model for contexts.
 * <p>
 * Columns:
 * <ul>
 * <li>Context
 * <li>Visibility of the context.
 * <li>Number of measures in the context.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
final class ContextsTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private final transient Environment environment;
    private static final String[] COLUMN_NAMES = { "Context", "Visibility", "Measures" };
    private static final int COLUMN_CONTEXT = 0;
    private static final int COLUMN_VISIBILITY = 1;
    private static final int COLUMN_MEASURES = 2;
    private final transient List<Context> contexts = new CopyOnWriteArrayList<>();
    private final transient Map<Context, Record> contextsToRecords = new HashMap<>();
    private final transient EnvironmentListener listener = new EnvironmentListener() {
        @Override
        public void processContextCreated(Context context) {
            createRecord(context);
        }

        @Override
        public void processContextChanged(Context context) {
            final int row = getRowIndex(context);
            if (row >= 0) {
                fireTableRowsUpdated(row, row);
            }
        }

        @Override
        public void processSourceCreated(Source source) {
            // Ignore
        }

        @Override
        public void processSourceChanged(Source source) {
            // Ignore
        }

        @Override
        public void processMeasureCreated(Context context,
                                          Measure measure) {
            // Ignore
        }

        @Override
        public void processMeasureChanged(Measure measure) {
            // Ignore
        }
    };

    public ContextsTableModel(Environment environment) {
        this.environment = environment;
        for (final Context context : environment.getContexts()) {
            createRecord(context);
        }
        environment.addListener(listener);
    }

    private static final class Record {
        private final Context context;
        private boolean visible;

        public Record(Context context,
                      boolean visible) {
            this.context = context;
            this.visible = visible;
        }

        public Context getContext() {
            return context;
        }

        public boolean isVisible() {
            return visible;
        }

        public void setVisible(boolean visible) {
            this.visible = visible;
        }
    }

    private synchronized void createRecord(Context context) {
        final Record r = new Record(context, true);
        contexts.add(context);
        contextsToRecords.put(context, r);
        final int index = getRowIndex(context);
        fireTableRowsInserted(index, index);
    }

    private Record getRecord(Context context) {
        return contextsToRecords.get(context);
    }

    private int getRowIndex(Context context) {
        return contexts.indexOf(context);
    }

    private Record getRecord(int rowIndex) {
        return contextsToRecords.get(contexts.get(rowIndex));
    }

    public Environment getEnvironment() {
        return environment;
    }

    public List<Context> getContexts() {
        return contexts;
    }

    public Context getContext(int id) {
        for (final Context context : contexts) {
            if (context.getId() == id) {
                return context;
            }
        }
        return null;
    }

    public boolean isVisible(Context context) {
        final Record r = getRecord(context);
        return r != null && r.isVisible();
    }

    public void setVisible(Context context,
                           boolean visible) {
        setValueAt(visible, getRowIndex(context), COLUMN_VISIBILITY);
    }

    public void setVisible(boolean visible) {
        for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++) {
            setValueAt(visible, rowIndex, COLUMN_VISIBILITY);
        }
    }

    @Override
    public int getRowCount() {
        return contextsToRecords.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        if (column >= 0 && column < getColumnCount()) {
            return COLUMN_NAMES[column];
        } else {
            return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case COLUMN_CONTEXT:
            return Context.class;
        case COLUMN_VISIBILITY:
            return Boolean.class;
        case COLUMN_MEASURES:
            return Integer.class;
        default:
            return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex,
                                  int columnIndex) {
        return columnIndex == COLUMN_VISIBILITY;
    }

    @Override
    public Object getValueAt(int rowIndex,
                             int columnIndex) {
        final Record r = getRecord(rowIndex);

        switch (columnIndex) {
        case COLUMN_CONTEXT:
            return r.getContext();
        case COLUMN_VISIBILITY:
            return r.isVisible();
        case COLUMN_MEASURES:
            return r.getContext().getMeasuresCount();
        default:
            return null;
        }
    }

    @Override
    public void setValueAt(Object value,
                           int rowIndex,
                           int columnIndex) {
        if (columnIndex == COLUMN_VISIBILITY) {
            final Record r = getRecord(rowIndex);
            r.setVisible((Boolean) value);
            fireTableRowsUpdated(rowIndex, rowIndex);
        }
    }

    @Override
    public String toString() {
        return getClass().getCanonicalName() + "<" + environment + ">";
    }
}