package cdc.perfs.ui.swing;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;

import cdc.perfs.core.Context;

/**
 * Rendering/Edition of Contexts.
 *
 * @author Damien Carbonne
 *
 */
final class ContextCell {
    private ContextCell() {
    }

    public static class Renderer implements TableCellRenderer, UIResource {
        private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

        protected final JLabel wComponent;

        public Renderer() {
            wComponent = new JLabel();
            wComponent.setOpaque(true);
            wComponent.setFont(wComponent.getFont().deriveFont(Font.PLAIN));
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            final Context context = (Context) value;
            if (isSelected) {
                wComponent.setForeground(table.getSelectionForeground());
                wComponent.setBackground(table.getSelectionBackground());
            } else {
                wComponent.setForeground(table.getForeground());
                wComponent.setBackground(table.getBackground());
            }

            if (hasFocus) {
                wComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                wComponent.setBorder(NO_FOCUS_BORDER);
            }

            if (context.isAlive()) {
                wComponent.setText("<html>" + context.getId() + ":" + context.getName() + "</html>");
            } else {
                wComponent.setText("<html><font color=\"#FF0000\">" + context.getId() + ":"
                        + context.getName()
                        + "</font></html>");
            }

            return wComponent;
        }
    }
}