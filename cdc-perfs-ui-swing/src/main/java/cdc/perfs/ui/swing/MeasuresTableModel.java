package cdc.perfs.ui.swing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.Source;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentKind;
import cdc.perfs.core.EnvironmentListener;
import cdc.perfs.core.Measure;
import cdc.perfs.core.MeasureStatus;
import cdc.ui.swing.EDTEventHandler1;
import cdc.ui.swing.EDTEventHandler2;

final class MeasuresTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private final transient Environment environment;
    private static final String[] COLUMN_NAMES =
            { "Context", "Source", "Details", "Status", "Level", "Depth", "Height", "Begin", "End", "Elapsed" };
    /**
     * List of measures.
     * <p>
     * Must be consistent with contexts.
     */
    private final transient List<Measure> measures;

    /**
     * List of contexts.
     * <p>
     * Must be consistent with measures.
     */
    private final transient List<Context> contexts;

    private final transient EDTEventHandler2<Context, Measure> createHandler = new EDTEventHandler2<Context, Measure>() {
        @Override
        protected void processInEDT(Context context,
                                    Measure measure) {
            final int row = measures.size();
            measures.add(measure);
            contexts.add(context);
            fireTableRowsInserted(row, row);
        }
    };

    private final transient EDTEventHandler1<Measure> updateHandler = new EDTEventHandler1<Measure>() {
        @Override
        protected void processInEDT(Measure measure) {
            final int row = getRowIndex(measure);
            if (row >= 0) {
                fireTableRowsUpdated(row, row);
            }
        }

        private int getRowIndex(Measure measure) {
            // This should only be called on measures that are very close to the end
            return measures.lastIndexOf(measure);
        }
    };

    private final transient EnvironmentListener listener = new EnvironmentListener() {
        @Override
        public void processContextCreated(Context context) {
            // Ignore
        }

        @Override
        public void processContextChanged(Context context) {
            // Ignore
        }

        @Override
        public void processSourceCreated(Source source) {
            // Ignore
        }

        @Override
        public void processSourceChanged(Source source) {
            // Ignore
        }

        @Override
        public void processMeasureCreated(Context context,
                                          Measure measure) {
            createHandler.process(context, measure);
        }

        @Override
        public void processMeasureChanged(Measure measure) {
            updateHandler.process(measure);
        }
    };

    public MeasuresTableModel(Environment environment) {
        this.environment = environment;

        this.measures = new ArrayList<>(environment.getMeasuresCount());
        this.contexts = new ArrayList<>(environment.getMeasuresCount());

        final Map<Measure, Context> measureToContext = new HashMap<>();
        // Add all existing measures
        for (final Context context : environment.getContexts()) {
            for (int index = 0; index < context.getRootMeasuresCount(); index++) {
                addMeasureRec(context,
                              context.getRootMeasure(index),
                              measureToContext);
            }
        }
        // Sort them
        Collections.sort(measures, Measure.BEGIN_COMPARATOR);
        // Now add corresponding contexts
        for (final Measure measure : measures) {
            contexts.add(measureToContext.get(measure));
        }

        if (environment.getKind() == EnvironmentKind.RUNTIME) {
            environment.addListener(listener);
        }
    }

    private void addMeasureRec(Context context,
                               Measure measure,
                               Map<Measure, Context> map) {
        measures.add(measure);
        map.put(measure, context);
        for (Measure child = measure.getFirstChild(); child != null; child = child.getNextSibling()) {
            addMeasureRec(context, child, map);
        }
    }

    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public int getRowCount() {
        return measures.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        if (column >= 0 && column < getColumnCount()) {
            return COLUMN_NAMES[column];
        } else {
            return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return Context.class;
        case 1:
            return Source.class;
        case 2:
            return String.class;
        case 3:
            return MeasureStatus.class;
        case 4:
            return MeasureLevel.class;
        case 5, 6:
            return Integer.class;
        case 7, 8, 9:
            return Long.class;
        default:
            return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex,
                                  int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex,
                             int columnIndex) {
        switch (columnIndex) {
        case 0:
            return contexts.get(rowIndex);
        case 1:
            return measures.get(rowIndex).getSource();
        case 2:
            return measures.get(rowIndex).getDetails();
        case 3:
            return measures.get(rowIndex).getStatus();
        case 4:
            return measures.get(rowIndex).getLevel();
        case 5:
            return measures.get(rowIndex).getDepth();
        case 6:
            return measures.get(rowIndex).getHeight();
        case 7:
            return measures.get(rowIndex).getRelativeBeginNanos();
        case 8:
            return measures.get(rowIndex).getRelativeEndOrCurrentNanos();
        case 9:
            return measures.get(rowIndex).getElapsedNanos();
        default:
            return null;
        }
    }
}