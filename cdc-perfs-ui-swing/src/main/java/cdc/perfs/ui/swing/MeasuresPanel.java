package cdc.perfs.ui.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.core.Context;
import cdc.perfs.core.MeasureStatus;
import cdc.ui.swing.SwingUtils;
import cdc.ui.swing.cells.EnumUi;
import cdc.ui.swing.cells.NumberUi;

/**
 * Panel dedicated to display of Measures.
 * <p>
 * It contains a table that displays for each measure:
 * <ul>
 * <li>Its context.
 * <li>Its source.
 * <li>Its details.
 * <li>Its status.
 * <li>Its level.
 * <li>Its height.
 * <li>Its begin time.
 * <li>Its end time.
 * <li>Its elapsed time.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
class MeasuresPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private final JTable wTable;

    public MeasuresPanel(MeasuresTableModel model) {
        setLayout(new GridBagLayout());

        final JScrollPane wScrollPane = new JScrollPane();
        {
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(0, 0, 0, 0);
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            add(wScrollPane, gbc);
        }

        wTable = new JTable();

        wScrollPane.setViewportView(wTable);

        wTable.setModel(model);
        // wTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        // wTable.getColumnModel().getColumn(1).setPreferredWidth(50);
        wTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        wTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Context
        wTable.setDefaultRenderer(Context.class, new ContextCell.Renderer());

        // Measure level
        final EnumUi.Settings<MeasureLevel> measureLevelSettings = new EnumUi.Settings<MeasureLevel>() {
            final Icon info = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-info.png");
            final Icon major = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-major.png");
            final Icon minor = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-minor.png");
            final Icon micro = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-micro.png");
            final Icon debug = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-debug.png");

            @Override
            public Icon getIcon(MeasureLevel value) {
                switch (value) {
                case DEBUG:
                    return debug;
                case INFO:
                    return info;
                case MAJOR:
                    return major;
                case MICRO:
                    return micro;
                case MINOR:
                    return minor;
                default:
                    return null;
                }
            }

            @Override
            public String getText(MeasureLevel value) {
                return value.getLabel();
            }
        };
        wTable.setDefaultRenderer(MeasureLevel.class,
                                  new EnumUi.LabelCellRenderer<>(MeasureLevel.class, measureLevelSettings));

        // Status
        final EnumUi.Settings<MeasureStatus> measureStatusSettings = new EnumUi.Settings<MeasureStatus>() {
            final Icon frozen = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-status-frozen.png");
            final Icon error = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-status-error.png");
            final Icon running = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-status-running.png");

            @Override
            public Icon getIcon(MeasureStatus value) {
                switch (value) {
                case FROZEN:
                    return frozen;
                case FROZEN_ON_ERROR:
                    return error;
                case RUNNING:
                    return running;
                default:
                    return null;
                }
            }

            @Override
            public String getText(MeasureStatus value) {
                return value.getLabel();
            }
        };
        wTable.setDefaultRenderer(MeasureStatus.class,
                                  new EnumUi.LabelCellRenderer<>(MeasureStatus.class, measureStatusSettings));

        // Integers
        final NumberUi.Settings<Integer> integerSettings = new NumberUi.Settings<>(Integer.class);
        wTable.setDefaultRenderer(Integer.class, new NumberUi.CellRenderer<>(integerSettings));

        // Longs
        final NumberUi.Settings<Long> longSettings = new NumberUi.Settings<>(Long.class);
        wTable.setDefaultRenderer(Long.class, new NumberUi.CellRenderer<>(longSettings));

        wTable.setAutoCreateRowSorter(true);
    }
}