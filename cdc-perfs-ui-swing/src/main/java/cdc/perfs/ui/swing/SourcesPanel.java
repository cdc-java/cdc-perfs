package cdc.perfs.ui.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import cdc.perfs.api.SourceLevel;
import cdc.perfs.ui.SourceDisplay;
import cdc.ui.swing.SwingUtils;
import cdc.ui.swing.cells.EnumUi;
import cdc.ui.swing.cells.IntegerUi;

/**
 * Panel dedicated to display and control of Sources.
 * <p>
 * It contains a table that displays for each source:
 * <ul>
 * <li>The source name.
 * <li>The max level of generated measures for the source.
 * <li>The display (normal, ...) of the source.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
class SourcesPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private final JTable wTable;

    public SourcesPanel(SourcesTableModel model) {
        setLayout(new GridBagLayout());

        final JScrollPane wScrollPane = new JScrollPane();
        {
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(0, 0, 0, 0);
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            add(wScrollPane, gbc);
        }

        wTable = new JTable();

        wScrollPane.setViewportView(wTable);

        wTable.setModel(model);
        wTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        wTable.getColumnModel().getColumn(1).setPreferredWidth(50);
        wTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        wTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Source level
        final EnumUi.Settings<SourceLevel> sourceLevelSettings = new EnumUi.Settings<SourceLevel>() {
            final Icon none = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-none.png");
            final Icon info = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-info.png");
            final Icon major = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-major.png");
            final Icon minor = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-minor.png");
            final Icon micro = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-micro.png");
            final Icon debug = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-level-debug.png");

            @Override
            public Icon getIcon(SourceLevel value) {
                switch (value) {
                case DEBUG:
                    return debug;
                case INFO:
                    return info;
                case MAJOR:
                    return major;
                case MICRO:
                    return micro;
                case MINOR:
                    return minor;
                case NONE:
                    return none;
                default:
                    return null;
                }
            }

            @Override
            public String getText(SourceLevel value) {
                return value.getLabel();
            }
        };
        wTable.setDefaultRenderer(SourceLevel.class,
                                  new EnumUi.LabelCellRenderer<>(SourceLevel.class, sourceLevelSettings));
        wTable.setDefaultEditor(SourceLevel.class,
                                new EnumUi.CellEditor2<>(SourceLevel.class, sourceLevelSettings));

        // Source display
        final EnumUi.Settings<SourceDisplay> sourceDisplaySettings = new EnumUi.Settings<SourceDisplay>() {
            final Icon hidden = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-display-hidden.png");
            final Icon normal = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-display-normal.png");
            final Icon highlighted = SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-display-highlighted.png");

            @Override
            public Icon getIcon(SourceDisplay value) {
                switch (value) {
                case HIDDEN:
                    return hidden;
                case NORMAL:
                    return normal;
                case HIGHLIGHTED:
                    return highlighted;
                default:
                    return null;
                }
            }

            @Override
            public String getText(SourceDisplay value) {
                return value.getLabel();
            }
        };
        wTable.setDefaultRenderer(SourceDisplay.class,
                                  new EnumUi.LabelCellRenderer<>(SourceDisplay.class, sourceDisplaySettings));
        wTable.setDefaultEditor(SourceDisplay.class,
                                new EnumUi.CellEditor2<>(SourceDisplay.class, sourceDisplaySettings));

        final IntegerUi.Settings integerSettings = new IntegerUi.Settings();
        integerSettings.setFormat("%,d");
        wTable.setDefaultRenderer(Integer.class, new IntegerUi.CellRenderer(integerSettings));

        wTable.setAutoCreateRowSorter(true);
    }
}