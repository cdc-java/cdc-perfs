package cdc.perfs.ui.swing;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import cdc.perfs.core.Context;
import cdc.ui.swing.SwingUtils;
import cdc.ui.swing.cells.BooleanUi;
import cdc.ui.swing.cells.IntegerUi;

/**
 * Panel dedicated to Context related controls.
 *
 * @author Damien Carbonne
 *
 */
class ContextsPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private final JTable wTable;

    public ContextsPanel(ContextsTableModel model) {
        setLayout(new GridBagLayout());

        final JScrollPane wScrollPane = new JScrollPane();
        {
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(0, 0, 0, 0);
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            add(wScrollPane, gbc);
        }

        wTable = new JTable();

        wScrollPane.setViewportView(wTable);

        wTable.setModel(model);
        wTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        wTable.getColumnModel().getColumn(1).setPreferredWidth(50);
        wTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        wTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        final BooleanUi.Settings booleanSettings = new BooleanUi.Settings();
        booleanSettings.setTrueIcon(SwingUtils.loadIcon("cdc/perfs/images/cdc-perfs-visible.png"));
        // settings.setFalseIcon(new ImageIcon("data/cdc-perfs-hidden.png"));
        booleanSettings.setTrueText(null);
        booleanSettings.setFalseText(null);
        wTable.setDefaultRenderer(Boolean.class, new BooleanUi.CellRenderer(booleanSettings));
        wTable.setDefaultEditor(Boolean.class, new BooleanUi.CellEditor(booleanSettings));

        wTable.setDefaultRenderer(Context.class, new ContextCell.Renderer());

        final IntegerUi.Settings integerSettings = new IntegerUi.Settings();
        integerSettings.setFormat("%,d");
        wTable.setDefaultRenderer(Integer.class, new IntegerUi.CellRenderer(integerSettings));

        wTable.setAutoCreateRowSorter(true);
    }
}