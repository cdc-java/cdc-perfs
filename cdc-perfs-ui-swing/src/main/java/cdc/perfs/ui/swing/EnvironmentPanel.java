package cdc.perfs.ui.swing;

import java.awt.BorderLayout;
import java.io.File;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentKind;
import cdc.perfs.core.io.IoExtension;
import cdc.perfs.core.io.IoMode;
import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.perfs.core.snapshot.SnapshotEnvironment;
import cdc.ui.swing.FileNameSuffixFilter;
import cdc.ui.swing.progress.SwingProgressController;

/**
 * Panel containing control and display of an environment:
 * <ul>
 * <li>Display and control of contexts</li>
 * <li>Display and control of sources</li>
 * <li>Display and control of chart (contexts and measures)</li>
 * </ul>
 *
 * @author D. Carbonne
 *
 */
public final class EnvironmentPanel extends JPanel {
    static final Logger LOGGER = LogManager.getLogger(EnvironmentPanel.class);
    private static final long serialVersionUID = 1L;
    final MainFrame wFrame;
    private final JToolBar wToolBar = new JToolBar();
    private final JButton wSnaphot;
    final JButton wSave;
    final JButton wSaveAs;
    private final ContextsTableModel contextsModel;
    private final SourcesTableModel sourcesModel;
    private final MeasuresTableModel measuresModel;
    private final ContextsPanel wContextsPanel;
    private final SourcesPanel wSourcesPanel;
    private final MeasuresPanel wMeasuresPanel;
    private final ControlledChartPanel wControlledChartPanel;
    private String filename;

    public EnvironmentPanel(MainFrame frame,
                            Environment environment) {
        super();
        wFrame = frame;
        setLayout(new BorderLayout(0, 0));

        add(wToolBar, BorderLayout.PAGE_START);

        if (environment.getKind() == EnvironmentKind.RUNTIME) {
            wSnaphot = new JButton(MainFrame.SNAPSHOT);
            wSnaphot.addActionListener(event -> {
                final SnapshotEnvironment snaphot = new SnapshotEnvironment(RuntimeEnvironment.getInstance());
                wFrame.addSnaphot(snaphot, null);
            });

            wSave = null;
            wSaveAs = null;
            wToolBar.add(wSnaphot);
        } else {
            wSnaphot = null;
            wSave = new JButton("Save");
            wSave.addActionListener(event -> save(wSave));
            wToolBar.add(wSave);

            wSaveAs = new JButton("Save As");
            wSaveAs.addActionListener(event -> saveAs(wSaveAs));
            wToolBar.add(wSaveAs);
        }

        contextsModel = new ContextsTableModel(environment);
        sourcesModel = new SourcesTableModel(environment);
        if (environment.getKind() == EnvironmentKind.SNAPSHOT) {
            measuresModel = new MeasuresTableModel(environment);
        } else {
            measuresModel = null;
        }

        final JSplitPane wSplitPane = new JSplitPane();
        wSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        wSplitPane.setOneTouchExpandable(true);
        wSplitPane.setDividerLocation(175);

        add(wSplitPane);

        final JTabbedPane wTabbedPane = new JTabbedPane();
        wSplitPane.setLeftComponent(wTabbedPane);

        wContextsPanel = new ContextsPanel(contextsModel);
        wTabbedPane.addTab("Contexts", wContextsPanel);

        wSourcesPanel = new SourcesPanel(sourcesModel);
        wTabbedPane.addTab("Sources", wSourcesPanel);

        if (environment.getKind() == EnvironmentKind.SNAPSHOT) {
            wMeasuresPanel = new MeasuresPanel(measuresModel);
            wTabbedPane.addTab("Measures", wMeasuresPanel);
        } else {
            wMeasuresPanel = null;
        }

        wControlledChartPanel = new ControlledChartPanel(contextsModel, sourcesModel);
        wSplitPane.setRightComponent(wControlledChartPanel);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        wFrame.removeSnapshot(this);
    }

    public Environment getEnvironment() {
        return wControlledChartPanel.getEnvironment();
    }

    public ContextsTableModel getContextsTableModel() {
        return contextsModel;
    }

    public ContextsPanel getContextsPanel() {
        return wContextsPanel;
    }

    public SourcesTableModel getSourcesTableModel() {
        return sourcesModel;
    }

    public SourcesPanel getSourcesPanel() {
        return wSourcesPanel;
    }

    public MeasuresTableModel getMeasuresTableModel() {
        return measuresModel;
    }

    public MeasuresPanel getMeasuresPanel() {
        return wMeasuresPanel;
    }

    public ControlledChartPanel getControlledChartPanel() {
        return wControlledChartPanel;
    }

    public String getFilename() {
        return filename;
    }

    void setFilename(String filename) {
        this.filename = filename;
        wFrame.processFilenameChange(this);
    }

    void save(JComponent parent) {
        if (getFilename() == null) {
            saveAs(parent);
        } else {
            saveAs(parent, getFilename(), false);
        }
    }

    void saveAs(JComponent parent) {
        final JFileChooser wChooser = new JFileChooser();

        for (final IoExtension ext : IoExtension.values()) {
            if (ext.isSupported(IoMode.EXPORT)) {
                final FileNameSuffixFilter filter =
                        new FileNameSuffixFilter(ext.getDescription() + " (*." + ext.getLabel() + ")", ext.getLabel());
                wChooser.addChoosableFileFilter(filter);
            }
        }
        final int result = wChooser.showSaveDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            saveAs(parent, wChooser.getSelectedFile().getPath(), true);
        }
    }

    private void saveAs(JComponent parent,
                        String filename,
                        boolean check) {
        final String path = IoExtension.fixFilename(filename);
        if (check) {
            final File file = new File(path);
            if (file.exists()) {
                final int result =
                        JOptionPane.showConfirmDialog(parent,
                                                      "File '" + path + "' already exists.\nOverwrite it?",
                                                      "File exists",
                                                      JOptionPane.WARNING_MESSAGE,
                                                      JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.CANCEL_OPTION) {
                    return;
                }
            }
        }

        final SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
            @Override
            protected String doInBackground() throws Exception {
                final SwingProgressController controller = new SwingProgressController(parent, "Save " + path);
                PerfsIo.save(getEnvironment(), path, controller);
                return path;
            }

            @Override
            protected void done() {
                try {
                    final String result = get();
                    setFilename(result);
                } catch (final ExecutionException e) {
                    LOGGER.catching(e);
                    final Throwable cause = e.getCause();
                    JOptionPane.showMessageDialog(parent,
                                                  "Failed to save '" + path + "'\n" + cause.getClass().getSimpleName() + " "
                                                          + cause.getMessage(),
                                                  null,
                                                  JOptionPane.ERROR_MESSAGE);
                } catch (final InterruptedException e) {
                    LOGGER.catching(e);
                    // Ignored at the moment as save can not (yet) be cancelled
                }
            }
        };
        worker.execute();
    }
}