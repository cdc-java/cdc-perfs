package cdc.perfs.ui.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cdc.ui.swing.SwingUtils;

class About extends JFrame {
    private static final long serialVersionUID = 1L;

    public About(String title) {
        setTitle("About " + title);
        setIconImage(SwingUtils.getApplicationImage());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        final JPanel wPanel = new JPanel();
        wPanel.setLayout(new GridBagLayout());
        wPanel.setOpaque(true);
        wPanel.setBackground(Color.WHITE);
        {
            final JLabel wLabel = new JLabel();
            wLabel.setFont(wLabel.getFont().deriveFont(Font.PLAIN));
            final StringBuilder builder = new StringBuilder();
            builder.append("<html>");
            builder.append("<b>" + title + "</b><br>");
            builder.append("<b>Version:</b> " + cdc.perfs.api.Config.VERSION + " (Swing)<br>");
            builder.append("<br>");
            builder.append("(c) Copyright 2010-2024, Damien Carbonne");
            builder.append("</html>");
            wLabel.setText(builder.toString());
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.anchor = GridBagConstraints.FIRST_LINE_START;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(5, 5, 5, 5);
            wPanel.add(wLabel, gbc);
        }

        setContentPane(wPanel);
        pack();
    }
}