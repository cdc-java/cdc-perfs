package cdc.perfs.core;

public final class TestNanoLongFloat {
    private TestNanoLongFloat() {
    }

    public static float nanoLongToSecondFloat(long nanos) {
        return (float) (nanos * 1.0E-9);
    }

    public static long secondFloatToNanoLong(float seconds) {
        return (long) (seconds * 1.0E9);
    }

    public static void main(String[] args) {
        // long errs = 0;
        for (long i = 0; i < 100000000000L; i++) {
            if (i % 1000000 == 0) {
                System.out.println(i / 1000000);
            }
            final float f = nanoLongToSecondFloat(i);
            final long j = secondFloatToNanoLong(f);
            final long abserr = Math.abs(j - i);
            final double relerr = (double) abserr / (double) i;
            if (relerr > 1.0e-5) {
                // errs++;
                // final double rate = (double) errs / (double) i;
                System.out.println(i + " - " + j + " : " + abserr + " " + relerr); // +
                                                                                   // "
                                                                                   // "
                                                                                   // +
                                                                                   // rate);
            }
        }
    }
}