package cdc.perfs.core;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.info.GraphLayout;
import org.openjdk.jol.vm.VM;

import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.perfs.core.runtime.RuntimeMeasure;

public final class Infos {
    private Infos() {
    }

    public static void main(String[] args) {
        System.out.println(VM.current().details());
        System.out.println(ClassLayout.parseClass(RuntimeMeasure.class).toPrintable());
        System.out.println(GraphLayout.parseInstance(RuntimeEnvironment.getInstance()).toPrintable());
    }
}