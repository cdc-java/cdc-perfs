package cdc.perfs.core;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.snapshot.SnapshotEnvironment;

class PerfsIoTest {
    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xml.gz",
            "cp.xml",
            "cp.xml.gz",
            "dat",
            "dat.gz"
    })
    void testDepth5(String ext) throws IOException {
        final File file = new File("target/" + getClass().getSimpleName() + "-5." + ext);
        PerfsIoSupport.generate(file, 5, 5);
        final SnapshotEnvironment env = PerfsIo.load(file);

        assertNotNull(env);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xml.gz",
            "cp.xml",
            "cp.xml.gz",
            "dat",
            "dat.gz"
    })
    void testDepth1(String ext) throws IOException {
        final File file = new File("target/" + getClass().getSimpleName() + "-1." + ext);
        PerfsIoSupport.generate(file, 5, 1);
        final SnapshotEnvironment env = PerfsIo.load(file);

        assertNotNull(env);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "dat"
    })
    void testSize(String ext) throws IOException {
        final int[] mul0 = { 1, 10, 100, 1000 };
        final int[] mul1 = { 1, 2, 5 };
        for (final int k0 : mul0) {
            for (final int k1 : mul1) {
                final int s = k0 * k1;
                final File file = new File("target/" + getClass().getSimpleName() + "-size-" + s + "." + ext);
                PerfsIoSupport.generate(file, s, 1);
                final SnapshotEnvironment env = PerfsIo.load(file);
                assertNotNull(env);
            }
        }
    }
}