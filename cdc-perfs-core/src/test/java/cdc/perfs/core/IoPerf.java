package cdc.perfs.core;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import cdc.io.utils.ProgressControllerInputStream;
import cdc.perfs.core.io.IoExtension;
import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.io.PerfsXml;
import cdc.perfs.core.io.PerfsXml.StAXLoader;
import cdc.perfs.core.snapshot.SnapshotEnvironment;
import cdc.util.events.ProgressController;
import cdc.util.lang.FailureReaction;
import cdc.util.time.Chronometer;

public final class IoPerf {
    private IoPerf() {
    }

    private static class Handler extends DefaultHandler2 {
        public Handler() {
            super();
        }
    }

    private static void parse(File file) throws IOException {
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            final Handler handler = new Handler();
            final SAXParser parser = factory.newSAXParser();
            final InputStream is = new BufferedInputStream(new FileInputStream(file));
            final InputSource source = new InputSource(is);
            source.setSystemId(file.getPath());
            parser.parse(source, handler);
        } catch (final SAXException | ParserConfigurationException e) {
            throw new IOException(e);
        }
    }

    private static void parse2(File file) throws IOException {
        try (Reader reader = new FileReader(file)) {
            final XMLInputFactory factory = XMLInputFactory.newInstance();
            final XMLStreamReader xmlsr = factory.createXMLStreamReader(reader);
            while (xmlsr.hasNext()) {
                xmlsr.next();
            }
            xmlsr.close();
        } catch (final XMLStreamException e) {
            throw new IOException(e);
        }
    }

    private static void print(String message,
                              File file,
                              double seconds,
                              int count) {
        System.out.println(String.format("%10s %-30s (%,9d kB) in %6.3f s (%,6d k/s)",
                                         message,
                                         file.getPath(),
                                         file.length() / 1000,
                                         seconds,
                                         (int) (count / seconds / 1000.0)));
    }

    private static void parse(File file,
                              int count) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        parse(file);
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        print("parsed", file, seconds, count);
    }

    private static void parse2(File file,
                               int count) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        parse2(file);
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        print("parsed 2", file, seconds, count);
    }

    private static Environment load(File file,
                                    int count) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final SnapshotEnvironment env = PerfsIo.load(file);
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        print("loaded", file, seconds, count);
        return env;
    }

    private static Environment loadC(File file,
                                     int count) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final ProgressControllerInputStream is = new ProgressControllerInputStream(file, ProgressController.VOID);
        final SnapshotEnvironment env = PerfsIo.load(is, file.getPath(), ProgressController.VOID);
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        print("loaded C", file, seconds, count);
        return env;
    }

    private static Environment load2(File file,
                                     int count) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final PerfsXml.StAXLoader loader = new StAXLoader(FailureReaction.WARN);
        final SnapshotEnvironment env = loader.load(file);
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        print("loaded 2", file, seconds, count);
        return env;
    }

    private static void saveC(Environment env,
                              File file,
                              int count) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        PerfsIo.save(env, file, ProgressController.VOID);
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        print("saved C", file, seconds, count);
    }

    public static void main(String[] args) throws IOException {

        final String[] bases = {
                // "/home/damien/z100k",
                // "/home/damien/z200k",
                // "/home/damien/z400k",
                "/home/damien/z800k"
        };

        for (final String base : bases) {
            System.out.println("-----------------------------------------------");
            final File ref = new File(base + PerfsIo.DOT + IoExtension.DAT.getLabel());
            final String testBase = base + "2";
            final File fileXml = new File(testBase + PerfsIo.DOT + IoExtension.XML.getLabel());
            final File fileXmlGz = new File(testBase + PerfsIo.DOT + IoExtension.XML_GZ.getLabel());
            final File fileCpXml = new File(testBase + PerfsIo.DOT + IoExtension.COMPACT_XML.getLabel());
            final File fileCpXmlGz = new File(testBase + PerfsIo.DOT + IoExtension.COMPACT_XML_GZ.getLabel());
            final File fileDat = new File(testBase + PerfsIo.DOT + IoExtension.DAT.getLabel());
            final File fileDatGz = new File(testBase + PerfsIo.DOT + IoExtension.DAT_GZ.getLabel());

            final Environment refEnv = PerfsIo.load(ref);
            final int count = refEnv.getMeasuresCount();
            System.out.println(count + " measures");
            System.out.println("-----------------------------------------------");

            for (int i = 0; i < 1; i++) {
                saveC(refEnv, fileDat, count);
                load(fileDat, count);

                saveC(refEnv, fileXml, count);
                load(fileXml, count);
                loadC(fileXml, count);
                load2(fileXml, count);
                parse(fileXml, count);
                parse2(fileXml, count);

                saveC(refEnv, fileCpXml, count);
                load(fileCpXml, count);
                loadC(fileCpXml, count);
                parse(fileCpXml, count);

                saveC(refEnv, fileDatGz, count);
                load(fileDatGz, count);
                loadC(fileDatGz, count);

                saveC(refEnv, fileXmlGz, count);
                load(fileXmlGz, count);
                loadC(fileXmlGz, count);

                saveC(refEnv, fileCpXmlGz, count);
                load(fileCpXmlGz, count);
                loadC(fileCpXmlGz, count);

                System.out.println();
            }
        }
    }
}