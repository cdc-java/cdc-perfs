package cdc.perfs.core;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cdc.io.compress.Compressor;
import cdc.io.xml.XmlWriter;
import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.api.Source;
import cdc.perfs.core.io.PerfsXml;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.util.debug.Memory;
import cdc.util.events.ProgressController;
import cdc.util.lang.InvalidStateException;
import cdc.util.stats.RunningStats;
import cdc.util.time.Chronometer;

public final class TestPerfs {

    static boolean xml = true;

    private TestPerfs() {
    }

    private static void print(XmlWriter w) {
        System.out.println("--------------------------------------------------------");
        try {
            final PerfsXml.Printer printer = new PerfsXml.Printer();
            printer.printDocument(RuntimeEnvironment.getInstance(), w, ProgressController.VOID);
        } catch (InvalidStateException | IOException e) {
            e.printStackTrace();
        }
        System.out.println();
    }

    public static void main(String[] args) throws IOException, InterruptedException, InvalidStateException {

        RuntimeEnvironment.getInstance().addListener(new EnvironmentListener() {
            @Override
            public void processContextCreated(Context context) {
                System.out.println("processContextCreated(" + context + ")");
            }

            @Override
            public void processContextChanged(Context context) {
                System.out.println("processContextChanged(" + context + ")");
            }

            @Override
            public void processSourceCreated(Source source) {
                System.out.println("processSourceCreated(" + source + ")");
            }

            @Override
            public void processSourceChanged(Source source) {
                System.out.println("processSourceChanged(" + source + ")");
            }

            @Override
            public void processMeasureCreated(Context context,
                                              Measure measure) {
                // System.out.println("processMeasureCreated(" + measure + ")");
            }

            @Override
            public void processMeasureChanged(Measure measure) {
                // System.out.println("processMeasureChanged(" + measure + ")");
            }
        });

        // System.out.println(GraphLayout.parseInstance(RuntimeEnvironment.getInstance()).toPrintable());

        final Source source = RuntimeEnvironment.getInstance().getSource(TestPerfs.class);

        // System.out.println(GraphLayout.parseInstance(RuntimeEnvironment.getInstance()).toPrintable());

        if (xml) {
            XmlWriter w = null;
            try {
                w = new XmlWriter(System.out);
            } catch (final UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (w != null) {
                w.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
                {
                    final RuntimeProbe probe1 =
                            RuntimeEnvironment.getInstance().createProbe(source, MeasureLevel.INFO);
                    probe1.start("Test 1");
                    print(w);
                    probe1.stop();
                    print(w);
                }

                {
                    final RuntimeProbe probe1 =
                            RuntimeEnvironment.getInstance().createProbe(source, MeasureLevel.INFO);
                    probe1.start("Test 2");
                    print(w);
                    probe1.stop();
                    probe1.stop();
                    print(w);
                }

                {
                    final RuntimeProbe probe1 =
                            RuntimeEnvironment.getInstance().createProbe(source, MeasureLevel.INFO);
                    probe1.start("Test 3");
                    print(w);
                    {
                        final RuntimeProbe probe2 =
                                RuntimeEnvironment.getInstance().createProbe(source, MeasureLevel.MAJOR);
                        probe2.start("Test 3.1");
                        print(w);
                        {
                            final RuntimeProbe probe3 =
                                    RuntimeEnvironment.getInstance().createProbe(source, MeasureLevel.MINOR);
                            probe3.start();
                            print(w);
                        }
                        probe2.stop();
                        print(w);
                    }
                    probe1.stop();
                    print(w);
                }
            }
        }
        // System.out.println(GraphLayout.parseInstance(RuntimeEnvironment.getInstance()).toPrintable());

        Thread.sleep(10000);

        // System.out.println(VM.current().details());

        final int[] sizes = { 1000, 10000, 100000, 1000000, 10000000 };
        for (final int size : sizes) {
            final Chronometer total = new Chronometer();
            final Chronometer local = new Chronometer();
            final RunningStats stats = new RunningStats();
            {
                Memory.warmUp();
                final long heap1 = Memory.usedMemory();
                total.start();
                final RuntimeProbe probe =
                        RuntimeEnvironment.getInstance().createProbe(source, MeasureLevel.INFO);
                for (int index = 0; index < size; index++) {
                    local.start();
                    probe.start();
                    probe.stop();
                    local.suspend();
                    stats.add(local.getElapsedNanos());
                }
                total.suspend();
                Memory.runGC();
                final long heap2 = Memory.usedMemory();
                System.out.println(TestPerfs.class.getCanonicalName());
                System.out.println("total: " + total);
                System.out.println(stats);
                final long bytes = heap2 - heap1;
                System.out.println(bytes + " average: " + ((double) bytes / (double) size));
            }
        }

        final PerfsXml.Printer printer = new PerfsXml.Printer();
        printer.save(RuntimeEnvironment.getInstance(), "perfs.xml", Compressor.NONE, ProgressController.VOID);

        Thread.sleep(100000);
    }
}