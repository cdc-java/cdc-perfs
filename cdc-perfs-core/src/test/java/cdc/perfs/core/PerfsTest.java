package cdc.perfs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.RuntimeManager;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.api.Source;
import cdc.perfs.core.runtime.RuntimeEnvironment;

class PerfsTest {
    @Test
    void testSources() {
        final Source source1 = RuntimeManager.getSource("Source1");
        assertEquals(source1, source1);
        assertEquals(source1, RuntimeManager.getSource("Source1"));
        assertSame(1, RuntimeEnvironment.getInstance().getSources().size());
    }

    @Test
    void testRunTimeProbe() {
        final Source source = RuntimeManager.getSource("Source1");
        final RuntimeProbe p1 = RuntimeManager.createProbe(source, MeasureLevel.INFO);
        assertEquals(p1.getLevel(), MeasureLevel.INFO);
        assertEquals(p1.getSource(), source);
        assertTrue(p1.isEnabled());
        final RuntimeProbe p2 = RuntimeManager.createProbe(source, MeasureLevel.INFO);
        assertTrue(p2.isEnabled());

        assertSame(0, RuntimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount());

        // p1.start("start 1");
        // assertTrue(RunTimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount()
        // == 1);
        // p1.stop();
        // assertTrue(RunTimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount()
        // == 1);
        //
        // p1.start("start 2");
        // assertTrue(RunTimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount()
        // == 2);
        // p1.stop();
        // assertTrue(RunTimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount()
        // == 2);

        p1.start("S-3");
        assertSame(1, RuntimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount());
        p2.start("S-3.1");
        assertSame(1, RuntimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount());
        p2.stop();
        assertSame(1, RuntimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount());
        p1.stop();
        assertSame(1, RuntimeEnvironment.getInstance().getCurrentContext().getRootMeasuresCount());
    }
}