package cdc.perfs.core;

import java.io.File;
import java.io.IOException;

import cdc.perfs.api.RuntimeManager;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.api.Source;
import cdc.perfs.api.SourceLevel;
import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.util.events.ProgressController;

class PerfsIoSupport {
    private static final Source SOURCE = RuntimeManager.getSource(PerfsIoSupport.class);

    static void generate(File file,
                         int numChildren,
                         int maxDepth) throws IOException {
        RuntimeEnvironment.getInstance().removeAllMeasures();
        RuntimeEnvironment.getInstance().getSource(PerfsIo.class).setMaxLevel(SourceLevel.NONE);
        for (int childIndex = 0; childIndex < numChildren; childIndex++) {
            recurse(numChildren, childIndex, 1, maxDepth);
        }
        PerfsIo.save(RuntimeEnvironment.getInstance(),
                     file,
                     ProgressController.VERBOSE);
    }

    private static void recurse(int numChildren,
                                int childIndex,
                                int depth,
                                int maxDepth) {
        final RuntimeProbe probe = RuntimeManager.createProbe(SOURCE);
        probe.start("recurse(" + numChildren + ", " + depth + ", " + maxDepth + ")#" + childIndex);

        if (depth < maxDepth) {
            for (int i = 0; i < numChildren; i++) {
                recurse(numChildren, i, depth + 1, maxDepth);
            }
        }

        probe.stop();
    }
}