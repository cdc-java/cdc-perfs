package cdc.perfs.core;

/**
 * Enumeration of possible environment kinds.
 *
 * @author Damien Carbonne
 */
public enum EnvironmentKind {
    /**
     * The environment is a living one.
     * There should be one of them in a running application.
     */
    RUNTIME,

    /**
     * The environment is a dead one.
     * They may be any number of them.
     */
    SNAPSHOT
}