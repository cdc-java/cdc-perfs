package cdc.perfs.core;

/**
 * Enumeration of possible positions of a measure relatively to a reference
 * time.
 *
 * @author Damien Carbonne
 */
public enum Position {
    /**
     * The measure started after the reference time OR stopped before it.
     */
    GREATER_OR_EQUAL,

    /**
     * The measure started before the reference time AND stopped after it.
     */
    EQUAL,

    /**
     * The measure stopped before the reference time OR started before it.
     */
    LESS_OR_EQUAL
}