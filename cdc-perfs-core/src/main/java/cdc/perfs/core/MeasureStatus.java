package cdc.perfs.core;

import cdc.util.encoding.Encoded;
import cdc.util.encoding.Encoder;
import cdc.util.encoding.Encoders;

/**
 * Enumeration of possible measure statuses.
 *
 * @author Damien Carbonne
 */
public enum MeasureStatus implements Encoded<Character> {
    /**
     * The measure has been started by the associated probe and is still
     * running.
     */
    RUNNING("Running", 'R'),

    /**
     * The measure has been normally ended by the associated probe and wont
     * evolve anymore.
     */
    FROZEN("Frozen", 'F'),

    /**
     * The measure has been closed because of an error:
     * <ul>
     * <li>Another higher level measure was ended before the associated one
     * is ended.</li>
     * <li>The associated probe was started and it was already running.</li>
     * <li>The associated probe was ended and it was not running.
     * This indicates that the associated probe forgot to end its measure.
     * </li>
     * </ul>
     */
    FROZEN_ON_ERROR("Error", 'E');

    private final String label;
    private final Character code;

    private MeasureStatus(String label,
                          char code) {
        this.label = label;
        this.code = code;
    }

    public static final Encoder<MeasureStatus, Character> ENCODER = Encoders.encoder(MeasureStatus.class, Character.class);

    @Override
    public Character getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    /**
     * @return True if this status is frozen (FROZEN or FROZEN_ON_ERROR).
     */
    public boolean isFrozen() {
        return this != RUNNING;
    }
}