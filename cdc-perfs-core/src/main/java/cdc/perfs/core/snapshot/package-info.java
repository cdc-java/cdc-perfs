/**
 * Implementations dedicated to Snapshot environments.
 *
 * @author Damien Carbonne
 */
package cdc.perfs.core.snapshot;