package cdc.perfs.core.snapshot;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.Source;
import cdc.perfs.core.Measure;
import cdc.perfs.core.MeasureStatus;
import cdc.perfs.core.impl.AbstractMeasure;
import cdc.perfs.core.impl.SourceImpl;

public final class SnapshotMeasure extends AbstractMeasure {
    protected SnapshotMeasure(Measure measure,
                              SnapshotMeasure parent,
                              SnapshotMeasure previous,
                              SnapshotContext context) {
        super(parent,
              previous,
              (SourceImpl) context.getEnvironment().getSource(measure.getSource().getName()),
              measure.getDetails(),
              measure.getAbsoluteBeginNanos(),
              measure.getAbsoluteEndNanos(),
              measure.getStatus(),
              measure.getLevel(),
              context);
    }

    SnapshotMeasure(SnapshotMeasure parent,
                    SnapshotMeasure previous,
                    Source source,
                    String details,
                    long begin,
                    long end,
                    MeasureStatus status,
                    MeasureLevel level,
                    SnapshotContext context) {
        super(parent,
              previous,
              (SourceImpl) source,
              details,
              begin,
              end,
              status,
              level,
              context);
    }

    @Override
    public long getElapsedNanos() {
        if (getStatus() == MeasureStatus.RUNNING) {
            return getEnvironment().getCurrentNanos() - begin;
        } else {
            return end - begin;
        }
    }
}