package cdc.perfs.core.snapshot;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import cdc.perfs.api.Source;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentKind;
import cdc.perfs.core.impl.AbstractContext;
import cdc.perfs.core.impl.AbstractEnvironment;

/**
 * Implementation of Environment for snapshots.
 *
 * @author Damien Carbonne
 *
 */
public final class SnapshotEnvironment extends AbstractEnvironment {
    private final List<SnapshotContext> contexts = new ArrayList<>();
    private final long elapsedNanos;

    /**
     * Create an empty SnapshotEnvironment.
     * <p>
     * This is intended for deserialization.
     *
     * @param refNanos The reference absolute nano time.
     * @param refInstant The reference date.
     * @param elapsedNanos The duration of this environment.
     */
    public SnapshotEnvironment(long refNanos,
                               Instant refInstant,
                               long elapsedNanos) {
        super(refNanos, refInstant);
        this.elapsedNanos = elapsedNanos;
    }

    /**
     * Create a SnapshotEnvironment by cloning another Environment.
     *
     * @param environment The environment to clone.
     */
    public SnapshotEnvironment(Environment environment) {
        super(environment.getReferenceNanos(),
              environment.getReferenceInstant());
        this.elapsedNanos = environment.getElapsedNanos();

        // Clone sources
        for (final Source source : environment.getSources()) {
            final Source clone = getSource(source.getName());
            clone.setMaxLevel(source.getMaxLevel());
        }

        // Clone contexts
        for (final Context context : environment.getContexts()) {
            createContext(context);
        }
    }

    @Override
    public EnvironmentKind getKind() {
        return EnvironmentKind.SNAPSHOT;
    }

    @Override
    public List<? extends AbstractContext> getContexts() {
        return contexts;
    }

    @Override
    public void removeDeadContexts() {
        final ArrayList<SnapshotContext> deads = new ArrayList<>();
        for (final SnapshotContext context : contexts) {
            if (!context.isAlive()) {
                deads.add(context);
            }
        }
        contexts.removeAll(deads);
    }

    @Override
    public long getCurrentNanos() {
        return refNanos + elapsedNanos;
    }

    @Override
    public long getElapsedNanos() {
        return elapsedNanos;
    }

    private SnapshotContext createContext(Context context) {
        final SnapshotContext clone = new SnapshotContext(this, context);
        contexts.add(clone);
        return clone;
    }

    public SnapshotContext createContext(int id,
                                         String name,
                                         boolean alive) {
        final SnapshotContext result = new SnapshotContext(this, id, name, alive);
        contexts.add(result);
        return result;
    }
}