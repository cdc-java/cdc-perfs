package cdc.perfs.core.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import cdc.io.compress.Compressor;
import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.api.Source;
import cdc.perfs.core.Environment;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.perfs.core.snapshot.SnapshotEnvironment;
import cdc.util.events.ProgressController;
import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

public final class PerfsIo {
    private static final Source SOURCE = RuntimeEnvironment.getInstance().getSource(PerfsIo.class);

    public static final String DOT = ".";
    private static final String DAT = IoExtension.DAT.getLabel();
    private static final String DAT_GZ = IoExtension.DAT_GZ.getLabel();
    private static final String XML = IoExtension.XML.getLabel();
    private static final String XML_GZ = IoExtension.XML_GZ.getLabel();
    private static final String COMPACT_XML = IoExtension.COMPACT_XML.getLabel();
    private static final String COMPACT_XML_GZ = IoExtension.COMPACT_XML_GZ.getLabel();
    private static final String CSV = IoExtension.CSV.getLabel();
    private static final String XLSX = IoExtension.XLSX.getLabel();
    private static final String ODS = IoExtension.ODS.getLabel();

    public enum XmlLoading {
        /** First XML reading format. */
        DATA,
        /** Second (fastest) XML reading format. */
        STAX
    }

    public static XmlLoading xmlLoading = XmlLoading.STAX;

    private PerfsIo() {
    }

    private static IOException unexpectedExtension(Object o) {
        return new IOException("Unexpected extension '" + o + "'");
    }

    private static String loadObject(Object o) {
        return "load(" + o + ")";
    }

    public static SnapshotEnvironment load(InputStream in,
                                           String systemId,
                                           ProgressController controller) throws IOException {
        final RuntimeProbe probe = RuntimeEnvironment.getInstance().createProbe(SOURCE, MeasureLevel.INFO);
        probe.start(loadObject(systemId));
        try {
            Checks.isNotNull(in, "in");
            Checks.isNotNull(systemId, "systemId");
            if (systemId.endsWith(DOT + DAT)) {
                return PerfsBin.Reader.load(in, systemId, Compressor.NONE);
            } else if (systemId.endsWith(DOT + DAT_GZ)) {
                return PerfsBin.Reader.load(in, systemId, Compressor.GZIP);
            } else if (systemId.endsWith(DOT + XML) || systemId.endsWith(DOT + COMPACT_XML)) {
                if (xmlLoading == XmlLoading.DATA) {
                    final PerfsXml.DataLoader loader = new PerfsXml.DataLoader(FailureReaction.WARN);
                    loader.setProgressController(controller);
                    return loader.loadXml(in,
                                          systemId);
                } else {
                    final PerfsXml.StAXLoader loader = new PerfsXml.StAXLoader(FailureReaction.WARN);
                    return loader.load(in, systemId);
                }
            } else if (systemId.endsWith(DOT + XML_GZ) || systemId.endsWith(DOT + COMPACT_XML_GZ)) {
                if (xmlLoading == XmlLoading.DATA) {
                    final PerfsXml.DataLoader loader = new PerfsXml.DataLoader(FailureReaction.WARN);
                    loader.setProgressController(controller);
                    return loader.loadXml(in,
                                          systemId,
                                          Compressor.GZIP);
                } else {
                    final PerfsXml.StAXLoader loader = new PerfsXml.StAXLoader(FailureReaction.WARN);
                    return loader.load(in, systemId, Compressor.GZIP);
                }
            } else {
                throw unexpectedExtension(systemId);
            }
        } finally {
            probe.stop();
        }
    }

    public static SnapshotEnvironment load(File file) throws IOException {
        final RuntimeProbe probe = RuntimeEnvironment.getInstance().createProbe(SOURCE, MeasureLevel.INFO);
        probe.start(loadObject(file));
        try {
            Checks.isNotNull(file, "file");
            if (file.getPath().endsWith(DOT + DAT)) {
                return PerfsBin.Reader.load(file.getPath(), Compressor.NONE);
            } else if (file.getPath().endsWith(DOT + DAT_GZ)) {
                return PerfsBin.Reader.load(file.getPath(), Compressor.GZIP);
            } else if (file.getPath().endsWith(DOT + XML) || file.getPath().endsWith(DOT + COMPACT_XML)) {
                if (xmlLoading == XmlLoading.DATA) {
                    final PerfsXml.DataLoader loader = new PerfsXml.DataLoader(FailureReaction.WARN);
                    return loader.loadXml(file.getPath());
                } else {
                    final PerfsXml.StAXLoader loader = new PerfsXml.StAXLoader(FailureReaction.WARN);
                    return loader.load(file);
                }
            } else if (file.getPath().endsWith(DOT + XML_GZ) || file.getPath().endsWith(DOT + COMPACT_XML_GZ)) {
                if (xmlLoading == XmlLoading.DATA) {
                    final PerfsXml.DataLoader loader = new PerfsXml.DataLoader(FailureReaction.WARN);
                    return loader.loadXml(file.getPath(),
                                          Compressor.GZIP);
                } else {
                    final PerfsXml.StAXLoader loader = new PerfsXml.StAXLoader(FailureReaction.WARN);
                    return loader.load(file, Compressor.GZIP);
                }
            } else {
                throw unexpectedExtension(file);
            }
        } finally {
            probe.stop();
        }
    }

    public static SnapshotEnvironment load(URL url) throws IOException {
        final RuntimeProbe probe = RuntimeEnvironment.getInstance().createProbe(SOURCE, MeasureLevel.INFO);
        probe.start(loadObject(url));
        try {
            Checks.isNotNull(url, "url");
            if (url.getPath().endsWith(DOT + DAT)) {
                return PerfsBin.Reader.load(url, Compressor.NONE);
            } else if (url.getPath().endsWith(DOT + DAT_GZ)) {
                return PerfsBin.Reader.load(url, Compressor.GZIP);
            } else if (url.getPath().endsWith(DOT + XML) || url.getPath().endsWith(DOT + COMPACT_XML)) {
                if (xmlLoading == XmlLoading.DATA) {
                    final PerfsXml.DataLoader loader = new PerfsXml.DataLoader(FailureReaction.WARN);
                    return loader.loadXml(url);
                } else {
                    final PerfsXml.StAXLoader loader = new PerfsXml.StAXLoader(FailureReaction.WARN);
                    return loader.load(url);
                }
            } else if (url.getPath().endsWith(DOT + XML_GZ) || url.getPath().endsWith(DOT + COMPACT_XML_GZ)) {
                if (xmlLoading == XmlLoading.DATA) {
                    final PerfsXml.DataLoader loader = new PerfsXml.DataLoader(FailureReaction.WARN);
                    return loader.loadXml(url, Compressor.GZIP);
                } else {
                    final PerfsXml.StAXLoader loader = new PerfsXml.StAXLoader(FailureReaction.WARN);
                    return loader.load(url, Compressor.GZIP);
                }
            } else {
                throw unexpectedExtension(url);
            }
        } finally {
            probe.stop();
        }
    }

    /**
     * Saves an environment to a file, auto-detecting format from extension.
     *
     * @param environment The environment.
     * @param filename The file name.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public static void save(Environment environment,
                            String filename,
                            ProgressController controller) throws IOException {
        final RuntimeProbe probe = RuntimeEnvironment.getInstance().createProbe(SOURCE, MeasureLevel.INFO);
        probe.start("save(" + filename + ")");
        try {
            if (filename.endsWith(DAT)) {
                PerfsBin.Writer.save(environment, filename, Compressor.NONE, controller);
            } else if (filename.endsWith(DAT_GZ)) {
                PerfsBin.Writer.save(environment, filename, Compressor.GZIP, controller);
            } else if (filename.endsWith(COMPACT_XML)) {
                // Check this before XML
                final PerfsXml.Printer printer = new PerfsXml.Printer(PerfsXml.Format.COMPACT);
                printer.save(environment, filename, Compressor.NONE, controller);
            } else if (filename.endsWith(XML)) {
                final PerfsXml.Printer printer = new PerfsXml.Printer();
                printer.save(environment, filename, Compressor.NONE, controller);
            } else if (filename.endsWith(COMPACT_XML_GZ)) {
                // Check this before XML_GZ
                final PerfsXml.Printer printer = new PerfsXml.Printer(PerfsXml.Format.COMPACT);
                printer.save(environment, filename, Compressor.GZIP, controller);
            } else if (filename.endsWith(XML_GZ)) {
                final PerfsXml.Printer printer = new PerfsXml.Printer();
                printer.save(environment, filename, Compressor.GZIP, controller);
            } else if (filename.endsWith(CSV) || filename.endsWith(XLSX) || filename.endsWith(ODS)) {
                final PerfsWorkbook.Printer printer = new PerfsWorkbook.Printer();
                printer.save(environment, filename, controller);
            } else {
                throw unexpectedExtension(filename);
            }
        } finally {
            probe.stop();
        }
    }

    /**
     * Saves an environment to a file, auto-detecting format from extension.
     *
     * @param environment The environment.
     * @param file The file.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public static void save(Environment environment,
                            File file,
                            ProgressController controller) throws IOException {
        save(environment, file.getPath(), controller);
    }
}