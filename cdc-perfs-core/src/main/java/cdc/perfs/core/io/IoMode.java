package cdc.perfs.core.io;

public enum IoMode {
    IMPORT,
    EXPORT
}