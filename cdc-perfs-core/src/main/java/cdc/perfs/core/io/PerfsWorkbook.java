package cdc.perfs.core.io;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.RuntimeManager;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.api.Source;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.Measure;
import cdc.perfs.core.MeasureStatus;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.time.RefTime;
import cdc.util.time.TimeUnit;

public final class PerfsWorkbook {
    private PerfsWorkbook() {
    }

    public static class Printer {
        private static final Logger LOGGER = LogManager.getLogger(PerfsWorkbook.Printer.class);
        private static final Source SRC = RuntimeManager.getSource(Printer.class);

        public Printer() {
            super();
        }

        public void save(Environment environment,
                         String filename,
                         ProgressController controller) throws IOException {
            final RuntimeProbe probe = RuntimeManager.createProbe(SRC, MeasureLevel.INFO);
            probe.start("save(" + filename + ")");
            final WorkbookWriterFactory factory = new WorkbookWriterFactory();
            try (final WorkbookWriter<?> writer = factory.create(new File(filename), WorkbookWriterFeatures.STANDARD_FAST)) {
                printDocument(environment, writer, controller);
            } finally {
                probe.stop();
            }
        }

        public void printDocument(Environment environment,
                                  WorkbookWriter<?> writer,
                                  ProgressController controller) throws IOException {
            LOGGER.trace("printDocument(...)");
            final ProgressSupplier progressSupplier = new ProgressSupplier(controller);
            printEnvironment(environment, writer, progressSupplier);
        }

        private void printEnvironment(Environment environment,
                                      WorkbookWriter<?> writer,
                                      ProgressSupplier progressSupplier) throws IOException {
            LOGGER.trace("printEnvironment(...)");
            progressSupplier.setTotal((long) environment.getSources().size() + environment.getMeasuresCount());

            // Meta data
            writer.beginSheet("Meta data");
            writer.beginRow(TableSection.HEADER);
            writer.addCells("Property", "Value");

            writer.beginRow(TableSection.DATA);
            writer.addCells("Environment Kind", environment.getKind().name());

            writer.beginRow(TableSection.DATA);
            writer.addCell("Ref Date");
            writer.addCell(PerfsXml.getDateFormat().format(Date.from(environment.getReferenceInstant())));

            writer.beginRow(TableSection.DATA);
            writer.addCell("Ref Nanos");
            writer.addCell(environment.getReferenceNanos());

            writer.beginRow(TableSection.DATA);
            writer.addCell("Elapsed Nanos");
            writer.addCell(environment.getElapsedNanos());

            writer.beginRow(TableSection.DATA);
            writer.addCell("Elapsed Seconds");
            writer.addCell(nanosToSeconds(environment.getElapsedNanos()));

            // Sources
            writer.beginSheet("Sources");
            writer.beginRow(TableSection.HEADER);
            writer.addCells("Source", "Max Level");
            for (final Source source : environment.getSources()) {
                printSource(source, writer, progressSupplier);
            }

            // Contexts and Measures
            writer.beginSheet("Contexts and Measures");
            writer.beginRow(TableSection.HEADER);
            writer.addCells("Context Id",
                            "Context Name",
                            "Context Alive",
                            "Source",
                            "Status",
                            "Level",
                            "Details",
                            "Begin Nanos",
                            "End Nanos",
                            "Begin Seconds",
                            "End Seconds",
                            "Elapsed Seconds");
            for (final Context context : environment.getContexts()) {
                printContext(context, writer, progressSupplier);
            }
        }

        private static void printSource(Source source,
                                        WorkbookWriter<?> writer,
                                        ProgressSupplier progressSupplier) throws IOException {
            LOGGER.trace("printSource(...)");

            progressSupplier.checkCancelled();
            progressSupplier.incrementValue();

            writer.beginRow(TableSection.DATA);
            writer.addCell(source.getName());
            writer.addCell(source.getMaxLevel());
        }

        private void printContext(Context context,
                                  WorkbookWriter<?> writer,
                                  ProgressSupplier progressSupplier) throws IOException {
            LOGGER.trace("printContext(...)");

            for (int index = 0; index < context.getRootMeasuresCount(); index++) {
                printMeasure(context, context.getRootMeasure(index), writer, progressSupplier);
            }
        }

        private static double nanosToSeconds(long nanos) {
            return RefTime.nanosToDouble(nanos, TimeUnit.SECOND);
        }

        private void printMeasure(Context context,
                                  Measure measure,
                                  WorkbookWriter<?> writer,
                                  ProgressSupplier progressSupplier) throws IOException {
            LOGGER.trace("printMeasure(...)");

            progressSupplier.checkCancelled();
            progressSupplier.incrementValue();

            writer.beginRow(TableSection.DATA);
            writer.addCell(context.getId());
            writer.addCell(context.getName());
            writer.addCell(context.isAlive());

            writer.addCell(measure.getSource().getName());
            writer.addCell(measure.getStatus().name());
            writer.addCell(measure.getLevel().name());
            writer.addCell(measure.getDetails());
            writer.addCell(measure.getAbsoluteBeginNanos());
            if (measure.getStatus() == MeasureStatus.RUNNING) {
                writer.addEmptyCell();
            } else {
                writer.addCell(measure.getAbsoluteEndNanos());
            }
            writer.addCell(nanosToSeconds(measure.getRelativeBeginNanos()));
            if (measure.getStatus() == MeasureStatus.RUNNING) {
                writer.addEmptyCell();
                writer.addEmptyCell();
            } else {
                writer.addCell(nanosToSeconds(measure.getRelativeEndNanos()));
                writer.addCell(nanosToSeconds(measure.getElapsedNanos()));
            }

            Measure child = measure.getFirstChild();
            while (child != null) {
                printMeasure(context, child, writer, progressSupplier);
                child = child.getNextSibling();
            }
        }
    }
}