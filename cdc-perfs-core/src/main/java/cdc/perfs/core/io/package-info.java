/**
 * Classes used to save/restore an Environment to/from a file.
 *
 * @author Damien Carbonne
 */
package cdc.perfs.core.io;