package cdc.perfs.core;

import cdc.perfs.api.Source;

/**
 * Interface implemented by objects that need to be notified of environment
 * events.
 *
 * @author Damien Carbonne
 *
 */
public interface EnvironmentListener {
    /**
     * Called to notify that a context has been created.
     *
     * @param context The newly created context.
     */
    public void processContextCreated(Context context);

    /**
     * Called when context has changed.
     * <ul>
     * <li>Alive
     * </ul>
     *
     * @param context The context that changed.
     */
    public void processContextChanged(Context context);

    /**
     * Called to notify that a source has been created.
     *
     * @param source The newly created source.
     */
    public void processSourceCreated(Source source);

    /**
     * Called to notify that a source has changed (MaxLevel, MeasuresCount).
     * <ul>
     * <li>MaxLevel
     * <li>MeasuresCount
     * </ul>
     *
     * @param source The source that changed.
     */
    public void processSourceChanged(Source source);

    public void processMeasureCreated(Context context,
                                      Measure measure);

    public void processMeasureChanged(Measure measure);
}