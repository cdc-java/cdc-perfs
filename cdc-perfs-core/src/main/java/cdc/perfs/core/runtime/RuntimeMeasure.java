package cdc.perfs.core.runtime;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.core.MeasureStatus;
import cdc.perfs.core.impl.AbstractMeasure;
import cdc.perfs.core.impl.SourceImpl;

public final class RuntimeMeasure extends AbstractMeasure {
    protected RuntimeMeasure(RuntimeMeasure parent,
                             RuntimeMeasure previous,
                             SourceImpl source,
                             String details,
                             long begin,
                             MeasureLevel level,
                             RuntimeContext context) {
        super(parent,
              previous,
              source,
              details,
              begin,
              Long.MAX_VALUE,
              MeasureStatus.RUNNING,
              level,
              context);

        context.addToFront(this);
    }

    @Override
    public long getElapsedNanos() {
        if (getStatus() == MeasureStatus.RUNNING) {
            return System.nanoTime() - begin;
        } else {
            return end - begin;
        }
    }

    final void freeze(MeasureFreezeMode mode,
                      RuntimeContext context) {
        assert (getStatus() == MeasureStatus.RUNNING);
        final boolean expected = (this == context.getNextParent());
        // If expected is true, then the frozen measure is the expected one,
        // that is the deepest running one.
        // Otherwise, the frozen measure is not the deepest one.
        // Deeper measures are still running, they should have been stopped.
        // This measure is normally frozen and deeper ones are frozen with
        // an error indication.

        context.moveFrontOver(this);
        setStatus(mode == MeasureFreezeMode.NORMAL ? MeasureStatus.FROZEN : MeasureStatus.FROZEN_ON_ERROR);
        end = (mode == MeasureFreezeMode.IMMEDIATE_ERROR ? begin : System.nanoTime());

        if (!expected) {
            freezeLastChild(end);
        }
    }
}