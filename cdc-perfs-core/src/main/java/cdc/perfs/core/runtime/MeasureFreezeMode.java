package cdc.perfs.core.runtime;

/**
 * Enumeration of measure freezing modes.
 *
 * @author Damien Carbonne
 *
 */
enum MeasureFreezeMode {
    /**
     * The measure was stopped correctly.
     */
    NORMAL,

    /**
     * The measure was stopped because another measure, above it, was stopped
     * whilst this one was running
     */
    ERROR,

    /**
     * The measure was stopped whilst it was not started.
     */
    IMMEDIATE_ERROR
}