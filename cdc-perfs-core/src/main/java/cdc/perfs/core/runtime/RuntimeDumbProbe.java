package cdc.perfs.core.runtime;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.core.impl.SourceImpl;

/**
 * Implementation of Probe that does nothing.
 *
 * @author Damien Carbonne
 *
 */
final class RuntimeDumbProbe extends AbstractRuntimeProbe {
    RuntimeDumbProbe(SourceImpl source,
                     MeasureLevel level) {
        super(source, level);
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public void start(String details) {
        // Ignore
    }

    @Override
    public void start() {
        // Ignore
    }

    @Override
    public void restart(String details) {
        // Ignore
    }

    @Override
    public void restart() {
        // Ignore
    }

    @Override
    public void stop() {
        // Ignore
    }
}