/**
 * Implementations dedicated to Runtime environment.
 *
 * @author Damien Carbonne
 */
package cdc.perfs.core.runtime;