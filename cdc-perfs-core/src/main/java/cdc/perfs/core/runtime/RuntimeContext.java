package cdc.perfs.core.runtime;

import java.util.ArrayList;
import java.util.List;

import cdc.perfs.core.impl.AbstractContext;

/**
 * Implementation of Runtime context.
 *
 * @author Damien Carbonne
 *
 */
public final class RuntimeContext extends AbstractContext {
    /** Thread associated to this context. */
    private final Thread thread;

    /**  Height of this context. */
    private int height = 0;

    private Boolean alive = null;

    /**
     * Stack of running measures.
     * <p>
     * Last measure, if any, is the next parent.
     */
    private final List<RuntimeMeasure> front = new ArrayList<>();

    /**
     * Frozen measure that should be the head of the next created measure.
     * This may be null.
     * It also normally corresponds to the last measure that was frozen.
     */
    private RuntimeMeasure nextHead = null;

    RuntimeContext(RuntimeEnvironment environment,
                   int id,
                   Thread thread) {
        super(environment, id, thread.getName());
        this.thread = thread;
    }

    /**
     * Returns the running measure that should be the parent of the next created
     * measure.
     * <p>
     * This is normally the last created measure.
     * It should never be null (except at Context initialization time, when
     * the root measure is created).
     * It also corresponds to the next measure that should be frozen.
     * If this is not the case, there is an application error.
     *
     * @return The measure that should be used as next parent.
     */
    RuntimeMeasure getNextParent() {
        return front.isEmpty() ? null : front.get(front.size() - 1);
    }

    void addToFront(RuntimeMeasure measure) {
        front.add(measure);
        height = Math.max(height, getFrontSize());
        nextHead = null;
    }

    /**
     * @return The size of running measures stack.
     */
    private int getFrontSize() {
        return front.size();
    }

    /**
     * Returns the index in front stack of a measure.
     *
     * @param measure The measure whose index is searched.
     * @return The index of measure in front stack.
     * @throws IllegalArgumentException When measure does not belong to front stack.
     */
    private int getFrontIndeOf(RuntimeMeasure measure) {
        for (int index = front.size() - 1; index >= 0; index--) {
            if (front.get(index) == measure) {
                return index;
            }
        }
        throw new IllegalArgumentException("Non related measure");
    }

    /**
     * Modifies front stack so that next parent is over a given measure.
     *
     * @param measure The measure whose parent must be next parent. This measure
     *            must belong to front stack.
     */
    void moveFrontOver(RuntimeMeasure measure) {
        final int newSize = getFrontIndeOf(measure);
        while (front.size() > newSize) {
            front.remove(front.size() - 1);
        }
        nextHead = measure;
    }

    RuntimeMeasure getNextHead() {
        return nextHead;
    }

    @Override
    public RuntimeEnvironment getEnvironment() {
        return (RuntimeEnvironment) super.getEnvironment();
    }

    @Override
    public Thread getThread() {
        return thread;
    }

    @Override
    public boolean isAlive() {
        final boolean tmp = getThread() != null && getThread().isAlive();
        if (this.alive == null || this.alive != tmp) {
            this.alive = tmp;
            if (!tmp) {
                getEnvironment().fireContextChanged(this);
            }
        }
        return tmp;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void removeAllMeasures() {
        removeMeasures();
        height = 0;
        front.clear();
        nextHead = null;
        getEnvironment().fireContextChanged(this);
    }

}