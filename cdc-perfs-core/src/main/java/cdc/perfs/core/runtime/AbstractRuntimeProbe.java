package cdc.perfs.core.runtime;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.core.impl.SourceImpl;

/**
 * Abstract base class of runtime probes.
 * <p>
 * Probes are used to create measures.
 * A real (smart) probe is created when measures can really be done.
 * When the measure is disabled, a dumb (possible cached and reused) probe can
 * be used.
 *
 * @author Damien Carbonne
 *
 */
abstract class AbstractRuntimeProbe implements RuntimeProbe {
    private final SourceImpl source;
    private final MeasureLevel level;

    /**
     * Create a probe for a source and a given level.
     *
     * @param source The source associated to the probe.
     * @param level The level of the measures done by the probe.
     */
    AbstractRuntimeProbe(SourceImpl source,
                         MeasureLevel level) {
        if (source == null) {
            throw new IllegalArgumentException("Invalid null source");
        }
        if (level == null) {
            throw new IllegalArgumentException("Invalid null level");
        }
        this.source = source;
        this.level = level;
    }

    @Override
    public final SourceImpl getSource() {
        return source;
    }

    @Override
    public final MeasureLevel getLevel() {
        return level;
    }
}