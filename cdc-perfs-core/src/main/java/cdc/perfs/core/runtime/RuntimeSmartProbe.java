package cdc.perfs.core.runtime;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.core.MeasureStatus;
import cdc.perfs.core.impl.SourceImpl;

/**
 * Implementation of Probe that really makes measures.
 *
 * @author Damien Carbonne
 *
 */
final class RuntimeSmartProbe extends AbstractRuntimeProbe {
    /** Context of the current measure. */
    private RuntimeContext context;
    /** Current measure. */
    private RuntimeMeasure measure;
    /** Is the associated source enabled at time of creation of the probe !!! */
    private final boolean enabled;

    RuntimeSmartProbe(SourceImpl source,
                      MeasureLevel level) {
        super(source, level);
        context = null;
        measure = null;
        enabled = source.isEnabled(level);
    }

    @Override
    public final boolean isEnabled() {
        return enabled;
    }

    public final RuntimeContext getContext() {
        return context;
    }

    public final boolean isRunning() {
        return measure != null;
    }

    @Override
    public final void start(String details) {
        if (isEnabled()) {
            if (measure != null && measure.getStatus() == MeasureStatus.RUNNING) {
                measure.freeze(MeasureFreezeMode.ERROR, context);
            }
            context = RuntimeEnvironment.INSTANCE.getCurrentContext();
            measure = createMeasure(details, context);
        }
    }

    @Override
    public final void start() {
        start(null);
    }

    @Override
    public final void restart(String details) {
        stop();
        start(details);
    }

    @Override
    public final void restart() {
        restart(null);
    }

    @Override
    public final void stop() {
        if (isEnabled()) {
            if (measure == null) {
                // This means that the caller of this probe calls stop() when
                // the probe is already stopped
                context = RuntimeEnvironment.INSTANCE.getCurrentContext();
                measure = createMeasure(null, context);
                measure.freeze(MeasureFreezeMode.IMMEDIATE_ERROR, context);
            } else if (measure.getStatus() == MeasureStatus.RUNNING) {
                // It is possible that the associated measure has already been
                // stopped by another probe because this one was stopped too
                // late, or the other too early.
                // So we stop it if it is not already stopped.
                measure.freeze(MeasureFreezeMode.NORMAL, context);
            }
            measure = null;
            context = null;
        }
    }

    private RuntimeMeasure createMeasure(String details,
                                         RuntimeContext context) {
        final RuntimeMeasure result = new RuntimeMeasure(context.getNextParent(),
                                                         context.getNextHead(),
                                                         getSource(),
                                                         details,
                                                         System.nanoTime(),
                                                         getLevel(),
                                                         context);
        context.getEnvironment().fireMeasureCreated(context, result);
        return result;
    }
}