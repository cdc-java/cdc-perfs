package cdc.perfs.core;

public enum SpanPosition {
    INSIDE,
    OUTSIDE,
    OVERLAP
}