/**
 * Shared classes used to implement Runtime and Snapshot environments.
 *
 * @author Damien Carbonne
 */
package cdc.perfs.core.impl;