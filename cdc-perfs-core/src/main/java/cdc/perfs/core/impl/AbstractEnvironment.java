package cdc.perfs.core.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.perfs.api.Source;
import cdc.perfs.core.Context;
import cdc.perfs.core.Environment;
import cdc.perfs.core.EnvironmentListener;
import cdc.perfs.core.Measure;
import cdc.util.lang.Checks;
import cdc.util.time.TimeMeasureMode;

/**
 * Base implementation of Environment.
 *
 * @author Damien Carbonne
 *
 */
public abstract class AbstractEnvironment implements Environment {
    private final Map<String, SourceImpl> nameToSource = new HashMap<>();
    private final List<EnvironmentListener> listeners = new ArrayList<>();
    protected final long refNanos;
    protected final Instant refInstant;

    /**
     * Creates an Environment.
     *
     * @param refNanos The reference absolute nano time.
     * @param refInstant The reference date, corresponding to refNanos.
     */
    protected AbstractEnvironment(long refNanos,
                                  Instant refInstant) {
        this.refNanos = refNanos;
        this.refInstant = refInstant;
    }

    @Override
    public final void addListener(EnvironmentListener listener) {
        if (listener != null && !listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public final void removeListener(EnvironmentListener listener) {
        listeners.remove(listener);
    }

    public final void fireContextCreation(Context context) {
        for (final EnvironmentListener listener : listeners) {
            listener.processContextCreated(context);
        }
    }

    public final void fireContextChanged(Context context) {
        for (final EnvironmentListener listener : listeners) {
            listener.processContextChanged(context);
        }
    }

    private void fireSourceCreation(Source source) {
        for (final EnvironmentListener listener : listeners) {
            listener.processSourceCreated(source);
        }
    }

    public final void fireSourceChanged(Source source) {
        for (final EnvironmentListener listener : listeners) {
            listener.processSourceChanged(source);
        }
    }

    public final void fireMeasureCreated(Context context,
                                         Measure measure) {
        for (final EnvironmentListener listener : listeners) {
            listener.processMeasureCreated(context, measure);
        }
    }

    protected final void fireMeasureChanged(Measure measure) {
        for (final EnvironmentListener listener : listeners) {
            listener.processMeasureChanged(measure);
        }
    }

    @Override
    public final long getReferenceNanos() {
        return refNanos;
    }

    @Override
    public final long toAbsolute(long time) {
        return time + refNanos;
    }

    @Override
    public final long toAbsolute(long time,
                                 TimeMeasureMode mode) {
        if (mode == TimeMeasureMode.ABSOLUTE) {
            return time;
        } else {
            return time + refNanos;
        }
    }

    @Override
    public final long toRelative(long time) {
        return time - refNanos;
    }

    @Override
    public final long toRelative(long time,
                                 TimeMeasureMode mode) {
        if (mode == TimeMeasureMode.RELATIVE) {
            return time;
        } else {
            return time - refNanos;
        }
    }

    @Override
    public final Instant getReferenceInstant() {
        return refInstant;
    }

    @Override
    public final void removeAllMeasures() {
        for (final Context context : getContexts()) {
            context.removeAllMeasures();
        }
    }

    @Override
    public final Source getSource(String name) {
        Checks.isNotNullOrEmpty(name, "name");
        SourceImpl result = nameToSource.get(name);
        if (result == null) {
            result = new SourceImpl(this, name);
            nameToSource.put(name, result);
            fireSourceCreation(result);
        }
        return result;
    }

    @Override
    public final Source getSource(Class<?> cls) {
        Checks.isNotNull(cls, "cls");
        String s = cls.getCanonicalName();
        // Do this for anonymous classes
        if (s == null) {
            s = cls.getName();
        }
        return getSource(s);
    }

    @Override
    public final Collection<SourceImpl> getSources() {
        return nameToSource.values();
    }

    @Override
    public String toString() {
        return getClass().getCanonicalName();
    }
}