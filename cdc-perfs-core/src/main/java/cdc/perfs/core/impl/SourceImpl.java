package cdc.perfs.core.impl;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.api.Source;
import cdc.perfs.api.SourceLevel;
import cdc.util.lang.Checks;

/**
 * Source implementation.
 *
 * @author Damien Carbonne
 */
public final class SourceImpl implements Source {
    /** Environment of the source. */
    private final AbstractEnvironment environment;
    /** Name of the source. */
    private final String name;
    /** Maximum level of measures. */
    private SourceLevel maxLevel = SourceLevel.MINOR;
    /** Number of measures associated to this source. */
    private int measuresCount = 0;

    protected SourceImpl(AbstractEnvironment environment,
                         String name) {
        this.environment = environment;
        this.name = name;
    }

    public AbstractEnvironment getEnvironment() {
        return environment;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setMaxLevel(SourceLevel maxLevel) {
        Checks.isNotNull(maxLevel, "maxLevel");

        this.maxLevel = maxLevel;
        environment.fireSourceChanged(this);
    }

    @Override
    public SourceLevel getMaxLevel() {
        return maxLevel;
    }

    @Override
    public boolean isEnabled(MeasureLevel level) {
        return maxLevel != SourceLevel.NONE
                && level != null
                && level.ordinal() <= maxLevel.toMeasureLevel().ordinal();
    }

    @Override
    public String toString() {
        return name;
    }

    public void setMeasuresCount(int count) {
        this.measuresCount = count;
        environment.fireSourceChanged(this);
    }

    public void incrementMeasuresCount() {
        measuresCount++;
        environment.fireSourceChanged(this);
    }

    @Override
    public int getMeasuresCount() {
        return measuresCount;
    }
}