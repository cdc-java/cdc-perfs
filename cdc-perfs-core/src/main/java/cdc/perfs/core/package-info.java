/**
 * Declaration of main types used to create measures.
 * <p>
 * An {@code Environment} is associated to an execution
 * environment.<br>
 * There is one living {@code RuntimeEnvironment} associated
 * to currently running VM.
 * New measures can be added to the RuntimeEnvironment.<br>
 * There are as many {@code SnapshotEnvironment}s as needed.
 * They are created by making snapshot of another environment or retrieved from
 * a saved Environment.
 * An Environment can be saved to a file. Loading such a file produces a
 * SnapshotEnvironment.
 * <p>
 * {@code Measure}s are created in a Context corresponding to a Thread and are
 * associated to a {@code Source}.
 * Each measure has a {@code Level} of importance.
 * <p>
 * A {@code Source} can correspond to a class, a package or anything else.
 * <p>
 * A {@code RuntimeProbe} is used to create {@code RuntimeMeasure}s.
 * <p>
 * In a {@code Context}, {@code Measure}s are organized as a tree.<br>
 * Starting and ending {@code Measure}s should be well balanced.<br>
 * If this not the case (wrong code, not handled exception, ...),
 * {@code Measure}s can be automatically ended or started.
 * Such a Measure can be recognized by its {@code FROZEN_ON_ERROR} status.
 * <p>
 * A typical usage follows:
 *
 * <pre>
 * class A {
 *     private static final Source SOURCE = RuntimeManager.getSource(A.class);
 *
 *     public void foo() {
 *         final RuntimeProbe probe = RuntimeManager.createProbe(SOURCE, MeasureLevel.INFO);
 *         probe.start("foo()");
 *         try {
 *            // Do something
 *         finally {
 *            probe.stop();
 *         }
 *     }
 * }
 * </pre>
 *
 * Creating probes and making measures has a cost (time and memory).
 * <p>
 * Setting maximum level of a source can be used to control memory usage.
 * Creating a measure for a disabled source has almost no memory cost (A cache
 * of shared probes is used for that).
 * Practically, except for most sensitive code, probes can be left in
 * production code.
 *
 * @author Damien Carbonne
 */
package cdc.perfs.core;