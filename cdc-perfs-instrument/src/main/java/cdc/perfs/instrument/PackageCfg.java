package cdc.perfs.instrument;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.perfs.api.MeasureLevel;
import cdc.util.lang.Checks;

/**
 * Configuration of package instrumentation.
 *
 * @author Damien Carbonne
 *
 */
public final class PackageCfg extends AbstractCfg {
    private boolean acceptsAllClasses = false;
    private final Map<String, ClassCfg> classes = new HashMap<>();

    public PackageCfg(String name) {
        super(name);
    }

    public void setAcceptsAllClasses(boolean value) {
        this.acceptsAllClasses = value;
    }

    public boolean acceptsAllClasses() {
        return acceptsAllClasses;
    }

    public ClassCfg addClass(ClassCfg cfg) {
        Checks.isNotNull(cfg, "cfg");

        classes.put(cfg.getName(), cfg);
        return cfg;
    }

    public Set<String> getClassNames() {
        return classes.keySet();
    }

    public ClassCfg getClass(String name) {
        return classes.get(name);
    }

    public boolean mustInstrumentClass(String simpleName) {
        if (acceptsAllClasses()) {
            return true;
        } else {
            final ClassCfg classCfg = getClass(simpleName);
            return classCfg != null;
        }
    }

    public MeasureLevel getMethodMeasureLevel(String simpleName,
                                              String methodName,
                                              String... argTypes) {
        if (acceptsAllClasses()) {
            return getLevel();
        } else {
            final ClassCfg classCfg = getClass(simpleName);
            return classCfg == null
                    ? null
                    : classCfg.getMethodMeasureLevel(methodName, argTypes);
        }
    }

    public boolean showArgs(ExtendedYesNo context,
                            String simpleName,
                            String methodName,
                            String... argTypes) {
        final ExtendedYesNo effective = context.mergeWithChild(getShowArgs());
        if (acceptsAllClasses() || effective.isStrong()) {
            return effective.isYes();
        } else {
            final ClassCfg classCfg = getClass(simpleName);
            return classCfg == null
                    ? false
                    : classCfg.showArgs(effective, methodName, argTypes);
        }
    }

    // @Override
    // public ExtendedYesNo getEffectiveShowArgs() {
    // return getShowArgs();
    // }

    @Override
    public String toString() {
        return "[Package " + getName()
                + (getLevel() == null ? "" : " " + getLevel())
                + (acceptsAllClasses ? " ALL_CLASSES" : "")
                + " " + getShowArgs()
                + "]";
    }
}