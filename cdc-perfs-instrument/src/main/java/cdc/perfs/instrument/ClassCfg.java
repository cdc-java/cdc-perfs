package cdc.perfs.instrument;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cdc.perfs.api.MeasureLevel;
import cdc.util.lang.Checks;

/**
 * Configuration of class instrumentation.
 *
 * @author Damien Carbonne
 *
 */
public final class ClassCfg extends AbstractCfg {
    private boolean acceptsAllMethods = false;
    private final Map<String, Set<MethodCfg>> methods = new HashMap<>();

    ClassCfg(String name) {
        super(name);
    }

    public void setAcceptsAllMethods(boolean value) {
        this.acceptsAllMethods = value;
    }

    public boolean acceptsAllMethods() {
        return acceptsAllMethods;
    }

    public MethodCfg addMethod(MethodCfg cfg) {
        Checks.isNotNull(cfg, "cfg");

        final Set<MethodCfg> set = methods.computeIfAbsent(cfg.getName(), name -> new HashSet<>());
        set.add(cfg);
        return cfg;
    }

    public Set<String> getMethodNames() {
        return methods.keySet();
    }

    public Set<MethodCfg> getMethods(String name) {
        return methods.get(name);
    }

    public Set<MethodCfg> getMethods() {
        final Set<MethodCfg> set = new HashSet<>();
        for (final Set<MethodCfg> tmp : methods.values()) {
            set.addAll(tmp);
        }
        return set;
    }

    public MeasureLevel getMethodMeasureLevel(String methodName,
                                              String... argTypes) {
        if (acceptsAllMethods()) {
            return getLevel();
        } else {
            final Set<MethodCfg> methodCfgs = getMethods(methodName);
            if (methodCfgs == null || methodCfgs.isEmpty()) {
                return null;
            } else {
                for (final MethodCfg methodCfg : methodCfgs) {
                    final MeasureLevel level = methodCfg.getMeasureLevel(argTypes);
                    if (level != null) {
                        return level;
                    }
                }
                return null;
            }
        }
    }

    public boolean showArgs(ExtendedYesNo context,
                            String methodName,
                            String... argTypes) {
        final ExtendedYesNo effective = context.mergeWithChild(getShowArgs());
        if (acceptsAllMethods() || effective.isStrong()) {
            return effective.isYes();
        } else {
            final Set<MethodCfg> methodCfgs = getMethods(methodName);
            if (methodCfgs == null || methodCfgs.isEmpty()) {
                return false;
            } else {
                for (final MethodCfg methodCfg : methodCfgs) {
                    if (methodCfg.matches(argTypes)) {
                        return effective.mergeWithChild(methodCfg.getShowArgs()).isYes();
                    }
                }
                return false;
            }
        }
    }

    @Override
    public String toString() {
        return "[Class " + getName()
                + " " + getLevel()
                + (acceptsAllMethods ? " ALL_METHODS" : "")
                + " " + getShowArgs()
                + "]";
    }
}