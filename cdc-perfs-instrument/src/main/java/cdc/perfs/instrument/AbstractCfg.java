package cdc.perfs.instrument;

import cdc.perfs.api.MeasureLevel;

/**
 * Base class for configuring performances instrumentation.
 *
 * @author Damien Carbonne
 *
 */
abstract class AbstractCfg extends AbstractNode {
    private final String name;
    private MeasureLevel level = null;

    protected AbstractCfg(String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public final MeasureLevel getLevel() {
        return level;
    }

    public final void setLevel(MeasureLevel level) {
        this.level = level;
    }
}