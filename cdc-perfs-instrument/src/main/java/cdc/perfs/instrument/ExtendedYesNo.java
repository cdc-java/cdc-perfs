package cdc.perfs.instrument;

public enum ExtendedYesNo {
    STRONG_NO,
    WEAK_NO,
    STRONG_YES,
    WEAK_YES,
    INHERITED;

    public boolean isYes() {
        return this == STRONG_YES || this == WEAK_YES;
    }

    public boolean isNo() {
        return this == STRONG_NO || this == WEAK_NO;
    }

    public boolean isStrong() {
        return this == STRONG_NO || this == STRONG_YES;
    }

    public boolean isWeak() {
        return this == WEAK_NO || this == WEAK_YES;
    }

    public ExtendedYesNo mergeWithChild(ExtendedYesNo child) {
        if (isStrong()) {
            return this;
        } else if (isWeak()) {
            if (child == INHERITED) {
                return this;
            } else {
                return child;
            }
        } else {
            throw new IllegalArgumentException();
        }
    }
}