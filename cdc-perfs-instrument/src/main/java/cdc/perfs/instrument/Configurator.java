package cdc.perfs.instrument;

import java.io.File;

import cdc.perfs.api.MeasureLevel;
import cdc.util.lang.Introspection;

/**
 * Interface used to provide instrumentation hints.
 *
 * @author Damien Carbonne
 */
public interface Configurator {

    /**
     * @return The file to use to backup perfs in the end.
     *         If {@code null}, no backup is executed.
     */
    public File getBackupFile();

    /**
     * Returns {@code true} if a class must be instrumented.
     *
     * @param packageName The class package name.
     * @param simpleName The class simple name.
     * @return {@code true} if the class named {@code packageName/simpleName} must be instrumented.
     */
    public boolean mustInstrumentClass(String packageName,
                                       String simpleName);

    /**
     * Returns {@code true} if a class must be instrumented.
     *
     * @param className The class name.
     * @return {@code true} if the class named {@code className} must be instrumented.
     */
    public default boolean mustInstrumentClass(String className) {
        return mustInstrumentClass(Introspection.getPackagePart(className),
                                   Introspection.getClassPart(className));
    }

    /**
     * Returns the name to use for the source static variable in a class.
     *
     * @param packageName The class package name.
     * @param simpleName The class simple name.
     * @return The name to use for the source static variable in class named {@code packageName/simpleName}.
     */
    public default String getSourceName(String packageName,
                                        String simpleName) {
        return "PROBE_SOURCE";
    }

    /**
     * Returns the MeasureLevel that must be used for a method.
     * <p>
     * A {@code null} result means that the method must not be instrumented.
     *
     * @param packageName The class package name.
     * @param simpleName The class simple name.
     * @param methodName The method name.
     * @param argClassNames Class names of method arguments.
     * @return The MeasureLevel that must be used for method whose signature is {@code methodSignature} in class named {@code className}.
     */
    public MeasureLevel getMethodMeasureLevel(String packageName,
                                              String simpleName,
                                              String methodName,
                                              String... argClassNames);

    /**
     * Returns {@code true} if a method must be instrumented.
     *
     * @param packageName The class package name.
     * @param simpleName The class simple name.
     * @param methodName The method name.
     * @param argClassNames Class names of method arguments.
     * @return {@code true} if method whose signature is {@code methodSignature} in class named {@code className} must be instrumented.
     */
    public default boolean mustInstrumentMethod(String packageName,
                                                String simpleName,
                                                String methodName,
                                                String... argClassNames) {
        return getMethodMeasureLevel(packageName, simpleName, methodName, argClassNames) != null;
    }

    public boolean mustShowMethodArgs(String packageName,
                                      String simpleName,
                                      String methodName,
                                      String... argClassNames);
}