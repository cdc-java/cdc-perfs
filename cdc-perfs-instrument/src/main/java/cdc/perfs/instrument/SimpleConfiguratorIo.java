package cdc.perfs.instrument;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.io.data.Element;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.perfs.api.MeasureLevel;
import cdc.util.lang.FailureReaction;

/**
 * Loading of the configuration of performances instrumentation.
 *
 * @author Damien Carbonne
 *
 */
public final class SimpleConfiguratorIo {
    private static final String ALL_CLASSES = "all-classes";
    private static final String ALL_MEHODS = "all-methods";
    private static final String ALL_ARGS = "all-args";
    private static final String ARG = "arg";
    private static final String BACKUP = "backup";
    private static final String CLASS = "class";
    private static final String PERFS_CONFIG = "perfs-config";
    private static final String LEVEL = "level";
    private static final String METHOD = "method";
    private static final String NAME = "name";
    private static final String PACKAGE = "package";
    private static final String SHOW_ARGS = "show-args";
    private static final String TYPE = "type";

    private SimpleConfiguratorIo() {
    }

    public static class DataLoader extends AbstractResourceLoader<SimpleConfigurator> {
        public DataLoader(FailureReaction reaction) {
            super(reaction);
        }

        @Override
        protected SimpleConfigurator loadRoot(Element root) {
            final SimpleConfigurator configurator = new SimpleConfigurator();
            if (PERFS_CONFIG.equals(root.getName())) {
                final ExtendedYesNo showArgs = root.getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.WEAK_YES);
                configurator.setShowArgs(showArgs);
                for (final Element child : root.getChildren(Element.class)) {
                    if (BACKUP.equals(child.getName())) {
                        final File file = new File(child.getText());
                        configurator.setBackupFile(file);
                    } else if (PACKAGE.equals(child.getName())) {
                        loadPackage(configurator, child);
                    } else {
                        unexpectedElement(child, BACKUP, PACKAGE);
                    }
                }
            } else {
                unexpectedElement(root, PERFS_CONFIG);
            }
            return configurator;
        }

        private void loadPackage(SimpleConfigurator configurator,
                                 Element element) {
            final String packageName = element.getAttributeValue(NAME);
            final ExtendedYesNo showArgs = element.getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.INHERITED);
            final PackageCfg packageCfg = new PackageCfg(packageName);
            packageCfg.setShowArgs(showArgs);
            configurator.addPackage(packageCfg);
            final Element allClasses = element.getChildAt(Element.class, Element.named(ALL_CLASSES), 0);

            if (allClasses == null) {
                packageCfg.setAcceptsAllClasses(false);
                for (final Element child : element.getChildren(Element.class)) {
                    if (CLASS.equals(child.getName())) {
                        loadClass(packageCfg, child);
                    } else {
                        unexpectedElement(child, CLASS);
                    }
                }
            } else {
                final MeasureLevel level = allClasses.getAttributeAsEnum(LEVEL, MeasureLevel.class, null);
                packageCfg.setAcceptsAllClasses(true);
                packageCfg.setLevel(level);
            }
        }

        private void loadClass(PackageCfg packageCfg,
                               Element element) {
            final String className = element.getAttributeValue(NAME);
            final ExtendedYesNo showArgs = element.getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.INHERITED);
            final ClassCfg classCfg = new ClassCfg(className);
            classCfg.setShowArgs(showArgs);
            packageCfg.addClass(classCfg);
            final Element allMethods = element.getChildAt(Element.class, Element.named(ALL_MEHODS), 0);

            if (allMethods == null) {
                classCfg.setAcceptsAllMethods(false);
                for (final Element child : element.getChildren(Element.class)) {
                    if (METHOD.equals(child.getName())) {
                        loadMethod(classCfg, child);
                    } else {
                        unexpectedElement(child, METHOD);
                    }
                }
            } else {
                final MeasureLevel level = allMethods.getAttributeAsEnum(LEVEL, MeasureLevel.class, null);
                classCfg.setAcceptsAllMethods(true);
                classCfg.setLevel(level);
            }
        }

        private void loadMethod(ClassCfg classCfg,
                                Element element) {
            final String methodName = element.getAttributeValue(NAME);
            final ExtendedYesNo showArgs = element.getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.INHERITED);
            final MeasureLevel level = element.getAttributeAsEnum(LEVEL, MeasureLevel.class, null);
            final MethodCfg methodCfg = new MethodCfg(methodName);
            methodCfg.setShowArgs(showArgs);
            methodCfg.setLevel(level);
            classCfg.addMethod(methodCfg);

            if (element.hasChildren(Element.class, Element.named(ALL_ARGS))) {
                methodCfg.setAcceptsAllArgs(true);
            } else {
                methodCfg.setAcceptsAllArgs(false);
                final List<String> types = new ArrayList<>();
                for (final Element child : element.getChildren(Element.class)) {
                    if (ARG.equals(child.getName())) {
                        final String type = child.getAttributeValue(TYPE);
                        types.add(type);
                    } else {
                        unexpectedElement(child, ARG);
                    }
                }
                methodCfg.setArgTypes(types.toArray(new String[0]));
            }
        }
    }

    public static class StAXLoader extends AbstractStAXLoader<SimpleConfigurator> {
        public StAXLoader(FailureReaction reaction) {
            super((reader,
                   systemId) -> new Parser(reader, systemId, reaction));
        }

        private static class Parser extends AbstractStAXParser<SimpleConfigurator> {
            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction) {
                super(reader, systemId, reaction);
            }

            @Override
            protected SimpleConfigurator parse() throws XMLStreamException {
                nextTag();

                if (isStartElement(PERFS_CONFIG)) {
                    final SimpleConfigurator result = parsePerfsConfig();
                    next();
                    return result;
                } else {
                    throw unexpectedEvent();
                }
            }

            private SimpleConfigurator parsePerfsConfig() throws XMLStreamException {
                final SimpleConfigurator configurator = new SimpleConfigurator();
                final ExtendedYesNo showArgs = getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.WEAK_YES);
                configurator.setShowArgs(showArgs);

                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(BACKUP)) {
                        parseBackup(configurator);
                    } else if (isStartElement(PACKAGE)) {
                        parsePackage(configurator);
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
                return configurator;
            }

            private void parseBackup(SimpleConfigurator configurator) throws XMLStreamException {
                final String filename = reader.getElementText();
                configurator.setBackupFile(new File(filename));
            }

            private void parsePackage(SimpleConfigurator configurator) throws XMLStreamException {
                final String name = getAttributeValue(NAME, null);
                final ExtendedYesNo showArgs = getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.INHERITED);
                final PackageCfg packageCfg = new PackageCfg(name);
                packageCfg.setShowArgs(showArgs);
                configurator.addPackage(packageCfg);
                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(ALL_CLASSES)) {
                        parseAllClasses(packageCfg);
                    } else if (isStartElement(CLASS)) {
                        parseClass(packageCfg);
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
            }

            private void parseAllClasses(PackageCfg packageCfg) throws XMLStreamException {
                final MeasureLevel level = getAttributeAsEnum(LEVEL, MeasureLevel.class, null);
                packageCfg.setAcceptsAllClasses(true);
                packageCfg.setLevel(level);
                nextTag();
            }

            private void parseClass(PackageCfg packageCfg) throws XMLStreamException {
                final String name = getAttributeValue(NAME, null);
                final ExtendedYesNo showArgs = getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.INHERITED);
                packageCfg.setAcceptsAllClasses(false);
                final ClassCfg classCfg = new ClassCfg(name);
                classCfg.setShowArgs(showArgs);
                packageCfg.addClass(classCfg);

                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(ALL_MEHODS)) {
                        parseAllMethods(classCfg);
                    } else if (isStartElement(METHOD)) {
                        parseMethod(classCfg);
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
            }

            private void parseAllMethods(ClassCfg classCfg) throws XMLStreamException {
                final MeasureLevel level = getAttributeAsEnum(LEVEL, MeasureLevel.class, null);
                classCfg.setAcceptsAllMethods(true);
                classCfg.setLevel(level);
                nextTag();
            }

            private void parseMethod(ClassCfg classCfg) throws XMLStreamException {
                final String name = getAttributeValue(NAME, null);
                final ExtendedYesNo showArgs = getAttributeAsEnum(SHOW_ARGS, ExtendedYesNo.class, ExtendedYesNo.INHERITED);
                final MeasureLevel level = getAttributeAsEnum(LEVEL, MeasureLevel.class, null);

                final MethodCfg methodCfg = new MethodCfg(name);
                methodCfg.setShowArgs(showArgs);
                methodCfg.setLevel(level);
                methodCfg.setAcceptsAllArgs(false);
                classCfg.addMethod(methodCfg);
                classCfg.setAcceptsAllMethods(false);

                List<String> types = null;

                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(ALL_ARGS)) {
                        parseAllArgs(methodCfg);
                    } else if (isStartElement(ARG)) {
                        if (types == null) {
                            types = new ArrayList<>();
                        }
                        parseArg(types);
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }

                if (!methodCfg.acceptslAllArgs()) {
                    if (types == null) {
                        types = new ArrayList<>();
                    }
                    methodCfg.setArgTypes(types.toArray(new String[0]));
                }
            }

            private void parseAllArgs(MethodCfg methodCfg) throws XMLStreamException {
                methodCfg.setAcceptsAllArgs(true);
                nextTag();
            }

            private void parseArg(List<String> types) throws XMLStreamException {
                final String type = getAttributeValue(TYPE, null);
                types.add(type);
                nextTag();
            }
        }
    }
}