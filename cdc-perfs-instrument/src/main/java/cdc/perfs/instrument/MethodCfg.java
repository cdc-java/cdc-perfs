package cdc.perfs.instrument;

import java.util.Arrays;

import cdc.perfs.api.MeasureLevel;

/**
 * Configuration of method instrumentation.
 *
 * @author Damien Carbonne
 *
 */
public final class MethodCfg extends AbstractCfg {
    private boolean acceptsAllArgs = false;
    private String[] argTypes = null;

    public MethodCfg(String name) {
        super(name);
    }

    public boolean acceptslAllArgs() {
        return acceptsAllArgs;
    }

    public void setAcceptsAllArgs(boolean value) {
        this.acceptsAllArgs = value;
    }

    public void setArgTypes(String... argTypes) {
        this.argTypes = argTypes.clone();
    }

    public String[] getArgTypes() {
        return argTypes;
    }

    public boolean matches(String... argTypes) {
        return Arrays.equals(this.argTypes, argTypes);
    }

    public MeasureLevel getMeasureLevel(String... argTypes) {
        if (acceptslAllArgs()) {
            return getLevel();
        } else {
            if (matches(argTypes)) {
                return getLevel();
            } else {
                return null;
            }
        }
    }

    @Override
    public String toString() {
        return "[Method " + getName()
                + " " + getLevel()
                + " " + (acceptslAllArgs() ? "ALL_ARGS" : Arrays.toString(argTypes))
                + " " + getShowArgs()
                + "]";
    }
}