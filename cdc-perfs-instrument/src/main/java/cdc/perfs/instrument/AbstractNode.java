package cdc.perfs.instrument;

abstract class AbstractNode {
    private ExtendedYesNo showArgs = ExtendedYesNo.INHERITED;

    protected AbstractNode() {
        super();
    }

    public final ExtendedYesNo getShowArgs() {
        return showArgs;
    }

    public void setShowArgs(ExtendedYesNo showArgs) {
        this.showArgs = showArgs;
    }
}