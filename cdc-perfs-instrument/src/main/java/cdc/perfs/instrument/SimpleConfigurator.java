package cdc.perfs.instrument;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.core.io.PerfsIo;
import cdc.perfs.core.runtime.RuntimeEnvironment;
import cdc.util.debug.Printable;
import cdc.util.events.ProgressController;
import cdc.util.lang.Checks;

/**
 * Simple implementation of Configurator.
 * <p>
 * Configuration can designate:
 * <ol>
 * <li>all methods of all classes in a package,
 * <li>all methods of a particular class in a package,
 * <li>all methods with a particular name in a class in a package,
 * <li>a specific method with a particular name and signature in a class in a package.
 * </ol>
 * Each time, a measure level can be associated.
 *
 * @author Damien Carbonne
 *
 */
public class SimpleConfigurator extends AbstractNode implements Configurator, Printable {
    private static final Logger LOGGER = LogManager.getLogger(SimpleConfigurator.class);
    private File backupFile = null;
    private final Map<String, PackageCfg> packages = new HashMap<>();

    public SimpleConfigurator() {
        super();
    }

    public void clear() {
        backupFile = null;
        packages.clear();
    }

    @Override
    public File getBackupFile() {
        return backupFile;
    }

    public void setBackupFile(File backupFile) {
        this.backupFile = backupFile;
    }

    public PackageCfg addPackage(PackageCfg cfg) {
        Checks.isNotNull(cfg, "cfg");
        packages.put(cfg.getName(), cfg);
        return cfg;
    }

    public Set<String> getPackageNames() {
        return packages.keySet();
    }

    public PackageCfg getPackage(String name) {
        return packages.get(name);
    }

    @Override
    public boolean mustInstrumentClass(String packageName,
                                       String simpleName) {
        final PackageCfg packageCfg = getPackage(packageName);
        final boolean result = packageCfg != null && packageCfg.mustInstrumentClass(simpleName);
        LOGGER.trace("mustInstrumentClass({}, {}) {}", packageName, simpleName, Boolean.valueOf(result));
        return result;
    }

    @Override
    public MeasureLevel getMethodMeasureLevel(String packageName,
                                              String simpleName,
                                              String methodName,
                                              String... argTypes) {
        final PackageCfg packageCfg = getPackage(packageName);
        final MeasureLevel result = packageCfg == null
                ? null
                : packageCfg.getMethodMeasureLevel(simpleName, methodName, argTypes);
        LOGGER.trace("getMethodMeasureLevel({}, {}, {}, ...) {}", packageName, simpleName, methodName, result);
        return result;
    }

    @Override
    public boolean mustShowMethodArgs(String packageName,
                                      String simpleName,
                                      String methodName,
                                      String... argClassNames) {
        if (getShowArgs().isStrong()) {
            return getShowArgs().isYes();
        } else {
            final PackageCfg packageCfg = getPackage(packageName);
            return packageCfg != null
                    && packageCfg.showArgs(getShowArgs(), simpleName, methodName, argClassNames);
        }
    }

    /**
     * Backups perfs to backup file if not {@code null}.
     */
    public void backupIfRequired() {
        if (backupFile != null) {
            try {
                final File parent = backupFile.getParentFile();
                if (parent == null || parent.isDirectory()) {
                    System.out.println("Save perfs to: " + backupFile);
                    PerfsIo.save(RuntimeEnvironment.getInstance(),
                                 backupFile,
                                 ProgressController.VOID);
                } else {
                    System.err.println("Non existing directory: " + parent);
                }
            } catch (final IOException e) {
                System.err.println("Failed to save perfs to " + backupFile + "\n" + e.getMessage());
            }
        }
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getShowArgs());
        for (final String packageName : getPackageNames()) {
            final PackageCfg packageCfg = getPackage(packageName);
            indent(out, level + 1);
            out.println(packageCfg);
            for (final String className : packageCfg.getClassNames()) {
                final ClassCfg classCfg = packageCfg.getClass(className);
                indent(out, level + 2);
                out.println(classCfg);
                for (final MethodCfg methodCfg : classCfg.getMethods()) {
                    indent(out, level + 3);
                    out.println(methodCfg);
                }
            }
        }
    }
}