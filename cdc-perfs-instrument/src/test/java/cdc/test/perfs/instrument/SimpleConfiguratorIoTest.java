package cdc.test.perfs.instrument;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.perfs.api.MeasureLevel;
import cdc.perfs.instrument.SimpleConfigurator;
import cdc.perfs.instrument.SimpleConfiguratorIo;
import cdc.util.lang.FailureReaction;

class SimpleConfiguratorIoTest {
    private static final Logger LOGGER = LogManager.getLogger(SimpleConfiguratorIoTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    private static void check(SimpleConfigurator configurator) {
        assertNotNull(configurator);
        assertEquals(new File("target/perfs.xml"), configurator.getBackupFile());

        configurator.print(OUT, 0);
        final Set<String> p = new HashSet<>();
        p.add("cdc/test/perfs/instrument/asm");
        p.add("p1");
        p.add("p2");
        p.add("p3");
        p.add("p4");
        assertEquals(p, configurator.getPackageNames());
        assertEquals(MeasureLevel.INFO, configurator.getMethodMeasureLevel("cdc/test/perfs/instrument/asm", "D1", "any"));
        assertEquals(MeasureLevel.INFO, configurator.getMethodMeasureLevel("p1", "Any", "any"));
        assertEquals(MeasureLevel.INFO, configurator.getMethodMeasureLevel("p2", "C", "any"));
        assertEquals(null, configurator.getMethodMeasureLevel("p2", "N", "any"));
        assertEquals(MeasureLevel.INFO, configurator.getMethodMeasureLevel("p3", "C", "m1"));
        assertEquals(null, configurator.getMethodMeasureLevel("p3", "C", "m2"));
        assertEquals(MeasureLevel.INFO, configurator.getMethodMeasureLevel("p4", "C", "m1", "java.lang.String"));
        // assertEquals(MeasureLevel.INFO, configurator.getMethodMeasureLevel("p4", "C", "m1", "String"));
        assertEquals(MeasureLevel.DEBUG, configurator.getMethodMeasureLevel("p4", "C", "m1"));
        assertEquals(null, configurator.getMethodMeasureLevel("p4", "C", "m2"));
    }

    @Test
    void testDataLoader() throws IOException {
        final SimpleConfiguratorIo.DataLoader loader = new SimpleConfiguratorIo.DataLoader(FailureReaction.FAIL);
        final SimpleConfigurator configurator = loader.loadXml("src/test/resources/config-with-backup.xml");
        check(configurator);
    }

    @Test
    void testStAXLoader() throws IOException {
        final SimpleConfiguratorIo.StAXLoader loader = new SimpleConfiguratorIo.StAXLoader(FailureReaction.FAIL);
        final SimpleConfigurator configurator = loader.load("src/test/resources/config-with-backup.xml");
        check(configurator);
    }
}